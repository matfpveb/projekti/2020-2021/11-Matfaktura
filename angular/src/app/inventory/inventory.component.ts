import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

const API_URL = 'http://localhost:8080/api/'

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  page = 1;
  count = 0;
  tableSize = 3;
  tableSizes = [3, 6, 9, 12];

  usluge: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.http.get(API_URL + 'usluga').subscribe(x => this.usluge = x);
  }

  deleteItem(id: any): void {
    if (confirm("Da li ste sigurni da želite da obrišete uslugu?")) {
      this.http.patch(API_URL + 'usluga/obrisi/', { inventoryId: id }).subscribe(response => {
        
        this.http.get(API_URL + 'usluga').subscribe(x => this.usluge = x);
      });
    }
  }

  onTableDataChange(event: any) {
    this.page = event;
    this.http.get(API_URL + 'usluga').subscribe(x => this.usluge = x);
  }

  onTableSizeChange(event: any): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.http.get(API_URL + 'usluga').subscribe(x => this.usluge = x);
  }

}

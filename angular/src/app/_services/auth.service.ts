import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';

const AUTH_API = 'http://localhost:8080/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient,
    private tokenStorage: TokenStorageService) { }

  isLoggedIn() {
    if (this.tokenStorage.getToken()) {
      return true;
    }
    return false;
  }

  login(
      login_email: string, 
      password: string
      ): Observable<any> {
    return this.http.post(AUTH_API + 'login', {
      login_email,
      password
    }, httpOptions);
  }

  register(
    login_email: string, 
    password: string, 
    naziv_firme: string,
    maticni_broj: number,
    pib: number,
    telefon: number,
    kontakt_email: string,
    website: string,
    adresa: string,
    grad: string,
    postanski_broj: number): 
    Observable<any> {
    return this.http.post(AUTH_API + 'register', {
      login_email, 
      password, 
      naziv_firme,
      maticni_broj,
      pib,
      telefon,
      kontakt_email,
      website,
      adresa,
      grad,
      postanski_broj
    }, httpOptions);
  }

}
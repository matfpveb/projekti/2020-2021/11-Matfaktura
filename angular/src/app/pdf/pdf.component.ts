import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { jsPDF } from "jspdf";
import { Router, ActivatedRoute } from "@angular/router";
import html2canvas from "html2canvas";

const API_URL = "http://localhost:8080/api/";

@Component({
  selector: "app-pdf",
  templateUrl: "./pdf.component.html",
  styleUrls: ["./pdf.component.css"]
})
export class PdfComponent implements OnInit {
  racun: any;
  racunId: any;
  tipRacuna: any;
  naslov: any;
  currentUsluge: any;
  currentOsnovica: any;
  currentPdv: any;
  currentUkupno: any;
  currentNalog: any;
  currentContainer: any;
  emailSent = false;


  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
  ) {   }

  ngOnInit(): void {
    this.racunId = this.route.snapshot.paramMap.get("id");
    this.tipRacuna = this.route.snapshot.paramMap.get("tip");
    this.http.get(API_URL + "predracuni/" + this.racunId).subscribe(
      x => {
        this.racun = x;

        let sumOsnovica: number = this.racun.usluge
          .map(
            (a: any) =>
              a.kolicina * a.proizvod_usluga_info.cena_po_jedinici_mere -
              (a.popust / 100) *
              a.kolicina *
              a.proizvod_usluga_info.cena_po_jedinici_mere
          )
          .reduce(function (a: any, b: any) {
            return a + b;
          });

        this.currentOsnovica = sumOsnovica;

        let sumPdv: number = this.racun.usluge
          .map(
            (a: any) =>
              ((a.kolicina * a.proizvod_usluga_info.cena_po_jedinici_mere -
                (a.popust / 100) *
                a.kolicina *
                a.proizvod_usluga_info.cena_po_jedinici_mere) *
                a.proizvod_usluga_info.pdv) /
              100
          )
          .reduce(function (a: any, b: any) {
            return a + b;
          });

        this.currentPdv = sumPdv;

        let sumUkupno: number = sumPdv + sumOsnovica;

        this.currentUkupno = sumUkupno;
      },
      err => {
        window.location.href = "/404";
      }
    );

    this.http.get(API_URL + "nalog").subscribe(x => {
      this.currentNalog = x;
    });

    if (this.tipRacuna) {
      this.initNaslov();
    }
  }
  initNaslov(): void {
    if (this.tipRacuna == "kr") {
      this.naslov = "RAČUN";
    // } else if (this.tipRacuna == "ar") {
    //   this.naslov = "AVANSNI RAČUN";
    } else if (this.tipRacuna == "pr") {
      this.naslov = "PREDRAČUN";
    } else {
      window.location.href = "/404";
    }
  }

  selected(selectedTheme: any) {

    if (selectedTheme == 0) {
      document.getElementById("container")!.className = "";
      document.getElementById("container")?.classList.add("container-fluid");
      this.currentContainer = "container-fluid";
    }
    if (selectedTheme == 1) {
      document.getElementById("container")!.className = "";
      document
        .getElementById("container")
        ?.classList.add("container-fluid-dark");
      this.currentContainer = "container-fluid-dark";
    }

    if (selectedTheme == 2) {
      document.getElementById("container")!.className = "";
      document
        .getElementById("container")
        ?.classList.add("container-fluid-matfaktura");
      this.currentContainer = "container-fluid-matfaktura";
    }

    if (selectedTheme == 3) {
      document.getElementById("container")!.className = "";
      document
        .getElementById("container")
        ?.classList.add("container-fluid-light");
      this.currentContainer = "container-fluid-light";
    }
  }

  generatePdf() {

    if (this.currentContainer === undefined) {
      this.currentContainer = "container-fluid";
    }

    let DATA = document.getElementsByClassName(
      this.currentContainer
    )[0] as HTMLElement;

    html2canvas(DATA, { scrollY: -window.scrollY }).then(canvas => {
      let fileWidth = 208;
      let fileHeight = (canvas.height * fileWidth) / canvas.width;

      const FILEURI = canvas.toDataURL("image.png");
      let PDF = new jsPDF("p", "mm", "a4");
      let position = 0;

      PDF.addImage(FILEURI, "PNG", 0, position, fileWidth, fileHeight);

      PDF.save(this.tipRacuna + "/" + this.racunId + ".pdf");
    });
  }
}

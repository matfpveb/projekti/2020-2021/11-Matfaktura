import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

const API_URL = 'http://localhost:8080/api/'
@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.css']
})
export class ClientProfileComponent implements OnInit {

  clientId: any;
  inventoryItem: any;
  isSuccessful = false;
  enableUpdate = false;


  clientForm: FormGroup;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    this.clientForm = new FormGroup({
      naziv: new FormControl('', [Validators.required]),
      maticni_broj: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      pib: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      ime_zastupnika: new FormControl('', [Validators.required]),
      adresa: new FormControl('', [Validators.required]),
      postanski_broj: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5)]),
      grad: new FormControl('', [Validators.required]),
      zemlja: new FormControl('', [Validators.required]),
      telefon: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
    this.clientId = this.route.snapshot.paramMap.get('id');
    if (this.clientId) {
      this.fillForm();
      this.enableUpdate = true;
    }
  }

  onSubmit(): void {
    if (this.clientForm.invalid) {
      window.alert("Nevalidna forma!");
      return;
    }
    if (this.clientId) {
      this.clientForm.value._id = this.clientId;
    }
    this.http.patch(API_URL + 'klijenti', this.clientForm.value).subscribe();
    this.router.navigate(['klijenti']);
  }

  fillForm(): void {
    this.http.get(API_URL + 'klijenti/' + this.clientId).subscribe(
      x => { this.clientForm.patchValue(x); },
      err => { this.router.navigate(['404']); }
    );
  }

  public nazivHasErrors(): string {
    return this.fieldHasErrors("naziv") ? "alert-danger" : "";
  }

  public maticniBrojHasErrors(): string {
    return this.fieldHasErrors("maticni_broj") ? "alert-danger" : "";
  }

  public pibHasErrors(): string {
    return this.fieldHasErrors("pib") ? "alert-danger" : "";
  }

  public imeZastupnikaHasErrors(): string {
    return this.fieldHasErrors("ime_zastupnika") ? "alert-danger" : "";
  }

  public postanskiBrojHasErrors(): string {
    return this.fieldHasErrors("postanski_broj") ? "alert-danger" : "";
  }

  public gradHasErrors(): string {
    return this.fieldHasErrors("grad") ? "alert-danger" : "";
  }

  public zemljaHasErrors(): string {
    return this.fieldHasErrors("zemlja") ? "alert-danger" : "";
  }

  public telefonHasErrors(): string {
    return this.fieldHasErrors("telefon") ? "alert-danger" : "";
  }

  public adresaHasErrors(): string {
    return this.fieldHasErrors("adresa") ? "alert-danger" : "";
  }

  public emailHasErrors(): string {
    return this.fieldHasErrors("email") ? "alert-danger" : "";
  }

  private fieldHasErrors(fieldName: string): boolean {
    const control: AbstractControl | null = this.clientForm.get(fieldName);
    if (control === null) {
      return false;
    }
    const errors: ValidationErrors | null = control.errors;
    return errors != null;
  }


  nazivErrors(): string[] {

    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('naziv');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Naziv klijenta je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }


  maticniBrojErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('maticni_broj');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }
  
    if(typeof(control.value) == "string"){
      errorMessages.push("Matični broj se mora sastojati samo od cifara.")
    }

    if (errors.required) {
      errorMessages.push("Matični broj je obavezan.");
    }

    if (errors.minlength || errors.maxlength) {
      errorMessages.push(`Matični broj mora imati tačno ${errors.minlength.requiredLength} cifara. Uneli ste ${errors.minlength.actualLength} cifara.`)
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  pibErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('pib');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("PIB je obavezan.");
    }

    if (errors.minlength || errors.maxlength) {
      errorMessages.push(`PIB mora imati tačno ${errors.minlength.requiredLength} cifara. Uneli ste ${errors.minlength.actualLength} cifara.`)
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  imeZastupnikaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('ime_zastupnika');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Ime zastupnika je obavezno.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  postanskiBrojErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('postanski_broj');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Poštanski broj je obavezan.");
    }

    if (errors.minlength || errors.maxlength) {
      errorMessages.push(`Poštanski broj mora imati tačno ${errors.minlength.requiredLength} cifara. Uneli ste ${errors.minlength.actualLength} cifara.`)
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  gradErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('grad');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Grad je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  zemljaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('zemlja');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Zemlja je obavezna.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  telefonErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('telefon');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Telefon je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  adresaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('adresa');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Adresa je obavezna.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  emailErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.clientForm.get('email');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Kontakt e-mail je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }


}


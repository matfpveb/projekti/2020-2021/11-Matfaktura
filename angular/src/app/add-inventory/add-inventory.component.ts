import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

const API_URL = 'http://localhost:8080/api/'

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.css']
})
export class AddInventoryComponent implements OnInit {

  enableUpdate = false;

  isPdvObveznik: any;
  stopePDV = [0];

  inventoryId: any;
  inventoryItem: any;

  inventoryForm: FormGroup;
  isSuccessful = false;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    this.inventoryForm = new FormGroup({
      naziv: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]),
      opis: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      cena_po_jedinici_mere: new FormControl('', [Validators.required]),
      jedinica_mere: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
      pdv: new FormControl('0', [Validators.required])
    })
  }

  ngOnInit(): void {

    this.initStopePdv();

    this.inventoryId = this.route.snapshot.paramMap.get('id');
    if (this.inventoryId) {
      this.fillForm();
      this.enableUpdate = true;
    }
  }

  onSubmit(): void {
    if (this.inventoryForm.invalid) {
      window.alert("Nevalidna forma!");
      return;
    }
    if (this.inventoryId) {
      this.inventoryForm.value._id = this.inventoryId;
    }
    this.http.patch(API_URL + 'usluga', this.inventoryForm.value).subscribe();
    this.router.navigate(['proizvodi-usluge']);
  }

  fillForm(): void {
    this.http.get(API_URL + 'usluga/' + this.inventoryId).subscribe(
      data => { this.inventoryForm.patchValue(data) },
      err => { this.router.navigate(['404']) }
    );
  }

  initStopePdv(): void {
    this.http.get(API_URL + 'nalog/pdv').subscribe(x => {
      this.isPdvObveznik = x;
      if (this.isPdvObveznik) {
        this.stopePDV.push(10);
        this.stopePDV.push(20);
      }
    });
  }

  deleteInventory(): void {
    if (confirm("Da li ste sigurni da želite da obrišete uslugu?")) {
      this.http.patch(API_URL + 'usluga/obrisi', { inventoryId: this.inventoryId }).subscribe(() => {
        this.router.navigate(['proizvodi-usluge']);
      });
    }
  }

  public nazivHasErrors(): string {
    return this.fieldHasErrors("naziv") ? "alert-danger" : "";
  }

  public jedinicaMereHasErrors(): string {
    return this.fieldHasErrors("jedinica_mere") ? "alert-danger" : "";
  }

  public cenaHasErrors(): string {
    return this.fieldHasErrors("cena_po_jedinici_mere") ? "alert-danger" : "";
  }

  public pdvHasErrors(): string {
    return this.fieldHasErrors("pdv") ? "alert-danger" : "";
  }

  public opisHasErrors(): string {
    return this.fieldHasErrors("opis") ? "alert-danger" : "";
  }

  private fieldHasErrors(fieldName: string): boolean {
    const control: AbstractControl | null = this.inventoryForm.get(fieldName);
    if (control === null) {
      return false;
    }
    const errors: ValidationErrors | null = control.errors;
    return errors != null;
  }

  nazivErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.inventoryForm.get('naziv');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Naziv je obavezan.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Naziv mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
        `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (errors.maxlength) {
      errorMessages.push(
        `Naziv može da sadrži najviše  ${errors.maxlength.requiredLength} karaktera. ` +
        `Uneli ste ${errors.maxlength.actualLength} karaktera.`
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  jedinicaMereErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.inventoryForm.get('jedinica_mere');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Jedinica mere je obavezna.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Jedinica mere mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
        `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (errors.maxlength) {
      errorMessages.push(
        `Jedinica mere može da sadrži najviše  ${errors.maxlength.requiredLength} karaktera. ` +
        `Uneli ste ${errors.maxlength.actualLength} karaktera.`
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  cenaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.inventoryForm.get('cena_po_jedinici_mere');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Cena po jedinici mere je obavezna.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  pdvErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.inventoryForm.get('pdv');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Pdv je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }


  opisErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.inventoryForm.get('opis');
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Opis je obavezan.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Opis mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
        `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (errors.maxlength) {
      errorMessages.push(
        `Opis može da sadrži najviše  ${errors.maxlength.requiredLength} karaktera. ` +
        `Uneli ste ${errors.maxlength.actualLength} karaktera.`
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }


}



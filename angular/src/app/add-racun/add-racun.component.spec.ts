import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRacunComponent } from './add-racun.component';

describe('AddRacunComponent', () => {
  let component: AddRacunComponent;
  let fixture: ComponentFixture<AddRacunComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRacunComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRacunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

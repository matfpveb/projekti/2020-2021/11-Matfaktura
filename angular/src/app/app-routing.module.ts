import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryComponent } from './inventory/inventory.component';
import { AddInventoryComponent } from './add-inventory/add-inventory.component';
import { ClientsComponent } from './clients/clients.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarLayoutComponent } from './_layouts/sidebar-layout/sidebar-layout.component';
import { LoginLayoutComponent } from './_layouts/login-layout/login-layout.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LoginActivateService } from './_services/login-activate.service';
import { NotfoundComponent } from './notfound/notfound.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { NalogInfoComponent } from './nalog-info/nalog-info.component';
import { AddRacunComponent } from './add-racun/add-racun.component';
import { RacuniComponent } from './racuni/racuni.component';
import { PdfComponent } from './pdf/pdf.component';

const routes: Routes = [

  // BITNO:
  // Rute se poklapaju po principu koja se prva nadje
  // Obavezno prvo navoditi najspecificnije / najduze rute
  // pa tek na kraju najopstije

  {
    path: '', component: SidebarLayoutComponent,
    canActivate: [LoginActivateService],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'klijenti/dodaj', component: ClientProfileComponent },
      { path: 'klijenti/:id', component: ClientProfileComponent },
      { path: 'klijenti', component: ClientsComponent },
      { path: 'proizvodi-usluge/dodaj', component: AddInventoryComponent },
      { path: 'proizvodi-usluge/:id', component: AddInventoryComponent },
      { path: 'proizvodi-usluge', component: InventoryComponent },
      { path: 'racuni/:tip/dodaj', component: AddRacunComponent },
      { path: 'racuni/:tip/pdf/:id', component: PdfComponent },
      { path: 'racuni/:tip/:id', component: AddRacunComponent },
      { path: 'racuni/:tip', component: RacuniComponent },      
      { path: 'nalog-info', component: NalogInfoComponent },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    ]
  },
  {
    path: '', component: LoginLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'registracija', component: RegisterComponent },
    ]
  },
  { path: '404', component: NotfoundComponent },
  { path: '**', redirectTo: '404' }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

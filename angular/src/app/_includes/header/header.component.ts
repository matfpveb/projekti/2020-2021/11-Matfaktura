import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../_services/token-storage.service';

const API_URL = 'http://localhost:8080/api/'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  nalog_info: any;
  constructor(private token: TokenStorageService, private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(API_URL + 'nalog').subscribe(x => this.nalog_info = x);
  }


  logout(): void {
    this.token.signOut();
    window.location.href = "/login";
  }

}

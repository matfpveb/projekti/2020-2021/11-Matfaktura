import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ChartsModule } from 'ng2-charts';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { SidebarComponent } from './_includes/sidebar/sidebar.component';
import { HeaderComponent } from './_includes/header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientsComponent } from './clients/clients.component';
import { InventoryComponent } from './inventory/inventory.component';
import { SidebarLayoutComponent } from './_layouts/sidebar-layout/sidebar-layout.component';
import { LoginLayoutComponent } from './_layouts/login-layout/login-layout.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { AddInventoryComponent } from './add-inventory/add-inventory.component';
import { AddRacunComponent } from './add-racun/add-racun.component';
import { NalogInfoComponent } from './nalog-info/nalog-info.component';
import { RacuniComponent } from './racuni/racuni.component';
import { PdfComponent } from './pdf/pdf.component';



@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    DashboardComponent,
    ClientsComponent,
    InventoryComponent,
    SidebarLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    RegisterComponent,
    NotfoundComponent,
    ClientProfileComponent,
    AddInventoryComponent,
    AddRacunComponent,
    NalogInfoComponent,
    RacuniComponent,
    PdfComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ChartsModule,
    NgxPaginationModule,
  ],

  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }

$(document).ready(function() {

    /* Main Chart */
    var bar = document.getElementById('mainChart').getContext('2d');
    var mainChart = new Chart(bar, {
        type: 'line',
        data: {
            labels: ['Oktobar 2020.', 'Novembar 2020.', 'Decembar 2020.', 'Januar 2021.', 'Februar 2021.', 'Mart 2021.'],
            datasets: [
                {
                    label: 'Zarada u $',
                    data: [5000, 3700, 2300, 5900, 12000, 13000],
                    backgroundColor: [
                        'red'
                    ],
                    borderColor: [
                        'red'
                    ],
                    borderWidth: 1
                }
            ]
        },
        options: {
            responsive: true,
            
        }
    });

    /* Pie Chart */
    var donut = document.getElementById('donutChart').getContext('2d');

    var donutData = {
        labels: ["Svetozar", "Dimitrije S.", "Dimitrije P.", "Mihailo", "Lazar"],
        datasets: [
            {
                fill: true,
                backgroundColor: [
                    'rgba(42, 145, 52, 1)',
                    'rgba(230, 57, 70, 1)',
                    'rgba(244, 162, 97, 1)',
                    'rgba(72, 149, 239, 1)',
                    'rgba(244, 243, 239, 1)'],
                data: [20, 20, 20, 20, 20],
                borderColor:	['rgba(42, 145, 52, .6)', 'rgba(230, 57, 70, .6)', 'rgba(244, 162, 97, .6)', 'rgba(72, 149, 239, .6)', 'black'],
                borderWidth: [2,2]
            }
        ]
    };

    var donutOptions = {
            title: {
                    display: true,
                    text: 'Udeo u projektu',
                    position: 'top'
                },
            rotation: -0.7 * Math.PI
    };
    var donutChart = new Chart(donut, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions
    });

    /* Line chart */
    var line = document.getElementById('lineChart').getContext('2d');

    var lineData = {
        labels: ["Mart", "April", "Maj", "Jun", "Jul", "Avgust"],
        datasets: [{
        label: "Zarada pre/posle projekta (hiljada)",
        data: [0, 0, 0, 20, 30, 155],
        backgroundColor: ['red'],
        borderColor: ['red']
        }]
    };
    
    var lineOptions = {
        legend: {
        display: true,
        position: 'top',
        labels: {
            boxWidth: 80,
            fontColor: 'black'
        }
        }
    };

    var lineChart = new Chart(line, {
        type: 'line',
        data: lineData,
        options: lineOptions
    });
});
// TODO maybe rename this file to verifyRegister?

const db = require("../models");
const Nalog = db.nalog;


checkDuplicateEmailOrPIB = (req, res, next) => {
  // Login email
  Nalog.findOne({
    login_email: req.body.login_email
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (user) {
      res.status(400).send({ message: "Failed! Email is already in use!" });
      return;
    }

    // Pib
    Nalog.findOne({
      pib: req.body.pib
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ message: "Failed! PIB is already in use!" });
        return;
      }

      next();
    });
  });
};

const verifySignUp = {
  checkDuplicateEmailOrPIB,
};

module.exports = verifySignUp;
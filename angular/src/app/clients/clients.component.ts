import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

const API_URL = 'http://localhost:8080/api/'


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  klijenti: any;
  page = 1;
  count = 0;
  tableSize = 3;
  tableSizes = [3, 6, 9, 12];

  isDashboard = false;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.http.get(API_URL + 'klijenti').subscribe(x => this.klijenti = x);

    if(this.router.url.includes("dashboard")){
      this.isDashboard = true;
    }
    
  }
  deleteKlijent(klijentId: any): void {
    if (confirm("Da li ste sigurni da želite da obrišete klijenta?")) {
      this.http.patch(API_URL + 'klijenti/obrisi', { klijentId: klijentId }).subscribe(response => {
        
        this.http.get(API_URL + 'klijenti').subscribe(x => this.klijenti = x);
      });
    }
  }

  onTableDataChange(event: any) {
    this.page = event;
    this.http.get(API_URL + 'klijenti').subscribe(x => this.klijenti = x);
  }

  onTableSizeChange(event: any): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.http.get(API_URL + 'klijenti').subscribe(x => this.klijenti = x);
  }
}
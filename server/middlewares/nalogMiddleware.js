const db = require('../models');
const Nalog = db.nalog;


getNalog = async (req, res, next) => {
    let nalog;
    try {
        nalog = await Nalog.findById(req.userId);
        if (nalog == null) {
            return res.status(404).json({ message: 'Cannot find nalog' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    req.nalog = nalog;
    next();
}

const nalogMiddleware = {
    getNalog
};

module.exports = nalogMiddleware;

// data will be pulled from database and iterated, this is just an example
var dataSet = [
    [ "<b>Firma01</b>", "932137123", "<a href='#'>Pera Perić</a>", "Neka ulica 52", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma02</b>", "932137123", "<a href='#'>BPera Perić2</a>", "Neka ulica 521", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma03</b>", "932137123", "<a href='#'>APera Perić3</a>", "Neka ulica 52", "11000", "Beograd", "DSrbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma04</b>", "932137123", "<a href='#'>CPera Perić4</a>", "Neka ulica 532", "21000", "Beograd", "FSrbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma05</b>", "932137123", "<a href='#'>DPera Perić5</a>", "Neka ulica 252", "31231", "BBeograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma06</b>", "932137123", "<a href='#'>Pera Perić6</a>", "Neka ulica 524", "11000", "DBeograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "apera123@gmail.com" ],
    [ "<b>Firma07</b>", "932137123", "<a href='#'>FPera Perić7</a>", "Neka ulica 152", "45462", "ABeograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma08</b>", "932137123", "<a href='#'>FPera Perić8</a>", "Neka ulica 52", "11000", "Beograd", "ASrbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma09</b>", "932137123", "<a href='#'>TPera Perić9</a>", "Neka ulica 552", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "dpera123@gmail.com" ],
    [ "<b>Firma10</b>", "932137123", "<a href='#'>TPera Perić10</a>", "Neka ulica 52", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma11</b>", "932137123", "<a href='#'>Pera Perić11</a>", "Neka ulica 572", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "fpera123@gmail.com" ],
    [ "<b>Firma12</b>", "932137123", "<a href='#'>Pera Perić12</a>", "Neka ulica 52", "11000", "NBeograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma13</b>", "932137123", "<a href='#'>Pera Perić13</a>", "Neka ulica 52", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "pera123@gmail.com" ],
    [ "<b>Firma14</b>", "932137123", "<a href='#'>Pera Perić14</a>", "Neka ulica 52", "11000", "Beograd", "Srbija", "<a href='tel:+641234567'>064/123-45-67</a>", "npera123@gmail.com" ],
    
];
 
$(document).ready(function() {
    $('#example').DataTable( {
        data: dataSet,
        columns: [
            { title: "Naziv" },
            { title: "PIB" },
            { title: "Ime zastupnika" },
            { title: "Adresa" },
            { title: "Poštanski broj" },
            { title: "Grad" },
            { title: "Zemlja" },
            { title: "Telefon" },
            { title: "E-mail" }
        ]
    } );
} );
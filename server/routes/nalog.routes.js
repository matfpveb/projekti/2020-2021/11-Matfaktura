const { authJwt, nalogMiddleware } = require("../middlewares");
const controller = require("../controllers/nalog.controller");
var multer = require('multer');

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
      callBack(null, '../angular/src/assets/uploads/logo')
    },
    filename: (req, file, callBack) => {
      callBack(null, `matfaktura_${file.originalname}`)
    }
  })
  const upload = multer({ storage: storage });

  app.get("/api/nalog", authJwt.verifyToken, nalogMiddleware.getNalog, controller.nalogInfo);
  app.patch("/api/nalog", authJwt.verifyToken, nalogMiddleware.getNalog, controller.nalogUpdate);
  app.get("/api/nalog/pdv", authJwt.verifyToken, nalogMiddleware.getNalog, controller.getPDV);
  app.patch("/api/nalog/pdv", authJwt.verifyToken, nalogMiddleware.getNalog, controller.updatePDV);
  app.patch('/api/nalog/logo', authJwt.verifyToken, nalogMiddleware.getNalog, upload.single('file'), controller.uploadLogo)

  app.patch("/api/usluga", authJwt.verifyToken, nalogMiddleware.getNalog, controller.dodajUslugu);
  app.get("/api/usluga", authJwt.verifyToken, nalogMiddleware.getNalog, controller.uslugeInfo);
  app.patch("/api/usluga/obrisi", authJwt.verifyToken, nalogMiddleware.getNalog, controller.deleteUsluga);
  app.get("/api/usluga/:id", authJwt.verifyToken, nalogMiddleware.getNalog, controller.getUslugaById);

  app.patch("/api/klijenti", authJwt.verifyToken, nalogMiddleware.getNalog, controller.dodajKlijenta);
  app.get("/api/klijenti", authJwt.verifyToken, nalogMiddleware.getNalog, controller.klijentiInfo);
  app.get("/api/klijenti/:id", authJwt.verifyToken, nalogMiddleware.getNalog, controller.getKlijentById);
  app.patch("/api/klijenti/obrisi", authJwt.verifyToken, nalogMiddleware.getNalog, controller.deleteKlijent);

  app.patch("/api/bankovni-racuni/dodaj", authJwt.verifyToken, nalogMiddleware.getNalog, controller.addBankovniRacun);
  app.patch("/api/bankovni-racuni/obrisi", authJwt.verifyToken, nalogMiddleware.getNalog, controller.deleteBankovniRacun);
  app.get("/api/bankovni-racuni", authJwt.verifyToken, nalogMiddleware.getNalog, controller.getBankovniRacuni);
};

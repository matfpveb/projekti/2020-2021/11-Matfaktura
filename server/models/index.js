const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.nalog = require("./nalog.model");
db.predracun = require("./predracun.model");

module.exports = db;
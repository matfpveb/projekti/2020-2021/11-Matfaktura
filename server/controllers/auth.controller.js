const config = require("../config/auth.config");
const db = require("../models");
const Nalog = db.nalog;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.register = (req, res) => {
    const nalog = new Nalog({
        login_email: req.body.login_email,
        password: bcrypt.hashSync(req.body.password, 10),
        naziv: req.body.naziv_firme,
        maticni_broj: req.body.maticni_broj,
        pib: req.body.pib,
        telefon: req.body.telefon,
        email: req.body.kontakt_email,
        website: req.body.website,
        adresa: req.body.adresa,
        grad: req.body.adresa,
        postanski_broj: req.body.postanski_broj,
        tip_identifikacije: "",
        pdv_obveznik: false,
        bankovni_racuni: [],
        klijenti: [],
        proizvod_usluga: []
    });

    try {
        nalog.save();
        res.send({ message: "User was registered successfully!" });
    }
    catch (e) {
        res.status(500).send({ message: e.message })
    }

    
};

exports.login = (req, res) => {
    Nalog.findOne({
        login_email: req.body.login_email
    }).exec((err, user) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!user) {
            return res.status(404).send({ message: "Korisnik nije pronađen." });
        }

        
        var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            user.password
        );

        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: "Neispravna lozinka!"
            });
        }

        var token = jwt.sign({ id: user.id }, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        res.status(200).send({
            id: user._id,
            login_email: user.login_email,
            accessToken: token
        });
    });
};

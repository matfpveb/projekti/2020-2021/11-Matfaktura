import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

const API_URL = 'http://localhost:8080/api/'

@Component({
  selector: 'app-add-racun',
  templateUrl: './add-racun.component.html',
  styleUrls: ['./add-racun.component.css']
})
export class AddRacunComponent implements OnInit {

  racunId: any;
  currentPredracun: any;
  currentUkOsnovica: number = 0;
  izdat = false;
  hasIzdatRacun = false;
  currentNalog: any;
  tipRacuna: any;


  racun = {
    broj: 0,
    mesto_izdavanja: '',
    datum_izdavanja: new Date().toLocaleDateString('de-DE'),
    mesto_prometa: '',
    datum_prometa: '',
    rok_placanja: '',
    ukupno_za_placanje: 0,
    // placeno: 0,
  };

  // klijent-form
  naslovRacuna = "";
  klijenti: any;
  selectedKlijent: any;
  selectedKlijentNaziv: any;
  klijentForm: any;

  // inventory-dynamic
  sveUsluge: any;
  dynamicInventoryForm!: FormGroup;
  selectedUsluga: any;
  currentKolicina: number = 0;
  currentPopust: number = 0;
  indexOfUsluga: any;
  currentFillingIndex: any;

  // forms data
  formDynamicData: any;
  formKlijentData: any;
  dataToSend: any;
  currentOsnovica: number = 0;
  currentUkupno: number = 0;
  currentUkupnaOsnovica: number = 0;
  currentUkupanPdv: number = 0;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    this.klijentForm = new FormGroup({
      klijenti: new FormControl(''),
      klijent_id: new FormControl(''),
      usluge: (this.fb.array([])),
      ukOsnovica: new FormControl(0),
      ukPdv: new FormControl(0),
      ukZaUplatu: new FormControl(0),
      prDatumIzdavanja: new FormControl(''),
      prMestoIzdavanja: new FormControl(''),
      prRokPlacanja: new FormControl(''),
      prNapomena: new FormControl(''),
      krDatumIzdavanja: new FormControl(''),
      krMestoIzdavanja: new FormControl(''),
      krDatumPrometa: new FormControl(''),
      krMestoPrometa: new FormControl(''),
      krRokPlacanja: new FormControl(''),
      krNapomena: new FormControl(''),
    });

    this.http.get(API_URL + 'nalog').subscribe(x => {
      this.currentNalog = x;
    });

    this.currentFillingIndex = 0;
    this.racunId = this.route.snapshot.paramMap.get('id');

    if (this.racunId) {
      this.fillFormFromId();
    } else {
      this.fillFormWithoutId();
    }

    this.tipRacuna = this.route.snapshot.paramMap.get('tip');
    if (this.tipRacuna) {
      this.initNaslov();
    }


    this.http.get(API_URL + 'usluga').subscribe(x => {
      this.sveUsluge = x;

    });

    this.http.get(API_URL + "klijenti").subscribe(x => {
      this.klijenti = x;
    });

  }

  selectKlijent(index: any) {
    this.selectedKlijent = this.klijenti[index];
    this.klijentForm.patchValue({ klijent_id: this.klijenti[index]._id })
  }

  napraviRacun(formDynamicData: any) {
    if (this.checkForm(formDynamicData)) {
      this.dataToSend = JSON.parse(JSON.stringify(formDynamicData));
      this.http.patch(API_URL + 'predracun', this.dataToSend).subscribe();
      this.router.navigate(['racuni/pr']);
    } else {
      alert("Molimo Vas popunite formu. Sva polja su obavezna.");
    }
  }

  checkForm(formDynamicData: any) {
    if (this.racunId) {
      return true;
    }

    if (formDynamicData.klijent_id == "") {
      return false;
    }

    if (formDynamicData.usluge.length == 0) {
      return false;
    }

    let errorCheck = true;
    formDynamicData.usluge.forEach((u: any) => {
      if (u.kolicina == "") {
        errorCheck = false;
      }
    });
    return errorCheck;

  }

  sacuvajPredracun(formDynamicData: any) {
    if (this.checkForm(formDynamicData)) {
      this.dataToSend = JSON.parse(JSON.stringify(formDynamicData));
      const izdavanjePredracunaData = {
        izdat: false,
        datum_izdavanja: '',
        mesto_izdavanja: this.klijentForm.get('prMestoIzdavanja').value,
        rok_placanja: this.klijentForm.get('prRokPlacanja').value,
        napomena: this.klijentForm.get('prNapomena').value,
      };

      this.http.patch(API_URL + 'predracun', { formData: this.dataToSend, izdavanjeData: izdavanjePredracunaData }).subscribe();
      this.router.navigate(['racuni/pr']);
    } else {
      alert("Molimo Vas popunite formu. Sva polja su obavezna.");
    }
  }

  izdajPredracun(formDynamicData: any) {
    if (this.checkForm(formDynamicData)) {
      this.dataToSend = JSON.parse(JSON.stringify(formDynamicData));

      const izdavanjePredracunaData = {
        izdat: true,
        datum_izdavanja: this.klijentForm.get('prDatumIzdavanja').value,
        mesto_izdavanja: this.klijentForm.get('prMestoIzdavanja').value,
        rok_placanja: this.klijentForm.get('prRokPlacanja').value,
        napomena: this.klijentForm.get('prNapomena').value,
      };



      if (this.racunId) {

        this.http.patch(API_URL + 'predracun/izdaj', { id: this.racunId, izdavanjeData: izdavanjePredracunaData }).subscribe();
      } else {

        this.http.patch(API_URL + 'predracun', { formData: this.dataToSend, izdavanjeData: izdavanjePredracunaData }).subscribe();
      }

      this.router.navigate(['racuni/pr']);
    } else {
      alert("Molimo Vas popunite formu. Sva polja su obavezna.");
    }
  }

  izdajRacunIzPredracuna(formDynamicData: any) {
    this.racun.mesto_izdavanja = this.currentNalog.grad;

    this.racun.datum_izdavanja = new Date().toLocaleDateString('de-DE');

    this.racun.mesto_prometa = this.racun.mesto_izdavanja;
    this.racun.datum_prometa = this.racun.datum_izdavanja;
    this.racun.rok_placanja = "30 dana";
    this.racun.ukupno_za_placanje = this.currentUkupno;


    this.http.patch(API_URL + "predracun/izdaj-racun", { id_predracuna: this.racunId, racun: this.racun }).subscribe();
    this.router.navigate(['racuni/kr']);
  }

  izdajRacun(formDynamicData: any) {
    if (this.checkForm(formDynamicData)) {
      this.dataToSend = JSON.parse(JSON.stringify(formDynamicData));


      this.http.patch(API_URL + 'racun/izdaj', { formData: this.dataToSend }).subscribe();

      this.router.navigate(['racuni/kr']);
    } else {
      alert("Molimo Vas popunite formu. Sva polja su obavezna.");
    }
  }


  // inventory-dynamic methods
  getIndex(index: any, pointIndex: any) {
    this.indexOfUsluga = index;
    this.currentFillingIndex = pointIndex;
    this.fillForm();
  }

  fillFormFromId() {
    this.http.get(API_URL + 'predracuni/' + this.racunId)
      .subscribe(
        x => {
          this.currentPredracun = x;
          this.izdat = this.currentPredracun.izdat;
          if (this.currentPredracun.racun) {
            this.hasIzdatRacun = true;
          }

          this.selectedKlijent = this.currentPredracun.klijent_info;
          this.currentPredracun.usluge.forEach((el: any) => {
            const osnovica = el.kolicina * el.proizvod_usluga_info.cena_po_jedinici_mere - el.popust / 100 * el.kolicina * el.proizvod_usluga_info.cena_po_jedinici_mere;
            this.usluge.push(this.fb.group({
              naziv: el.proizvod_usluga_info.naziv,
              jm: el.proizvod_usluga_info.jedinica_mere,
              cenaJm: el.proizvod_usluga_info.cena_po_jedinici_mere,
              kolicina: el.kolicina,
              popust: el.popust,
              osnovica: osnovica,
              stopaPdv: el.proizvod_usluga_info.pdv,
              pdv: osnovica * el.proizvod_usluga_info.pdv / 100,
              ukupno: osnovica + osnovica * el.proizvod_usluga_info.pdv / 100
            }))
          });
          this.updatePdv(0);
          this.updateOsnovica(0);
          this.updateUkupno(0);

          this.klijentForm.patchValue({ prNapomena: this.currentPredracun.napomena });
          this.klijentForm.patchValue({ prRokPlacanja: this.currentPredracun.rok_placanja });
          this.klijentForm.patchValue({ prMestoIzdavanja: this.currentPredracun.mesto_izdavanja });

          this.klijentForm.patchValue({ krMestoIzdavanja: this.currentPredracun.racun.mesto_izdavanja });
          this.klijentForm.patchValue({ krDatumIzdavanja: this.currentPredracun.racun.datum_izdavanja });
          this.klijentForm.patchValue({ krMestoPrometa: this.currentPredracun.racun.mesto_prometa });
          this.klijentForm.patchValue({ krDatumPrometa: this.currentPredracun.racun.datum_prometa });
          this.klijentForm.patchValue({ krRokPlacanja: this.currentPredracun.racun.rok_placanja });
          this.klijentForm.patchValue({ krNapomena: this.currentPredracun.racun.napomena });


          if (!this.izdat) {
            this.klijentForm.patchValue({ prDatumIzdavanja: new Date().toLocaleDateString('de-DE') });

          } else {
            this.klijentForm.patchValue({ prDatumIzdavanja: this.currentPredracun.datum_izdavanja });
          }
        },
        err => {
          this.router.navigate(['404']);
        }
      );
  }

  fillFormWithoutId() {
    this.klijentForm.patchValue({ prDatumIzdavanja: new Date().toLocaleDateString('de-DE') });
    this.klijentForm.patchValue({ krDatumIzdavanja: new Date().toLocaleDateString('de-DE') });
    this.klijentForm.patchValue({ krDatumPrometa: new Date().toLocaleDateString('de-DE') });

  }


  fillForm() {
    const currentIndex = this.currentFillingIndex;

    this.usluge.at(currentIndex).patchValue({ cenaJm: this.sveUsluge[this.indexOfUsluga].cena_po_jedinici_mere });
    this.usluge.at(currentIndex).patchValue({ jm: this.sveUsluge[this.indexOfUsluga].jedinica_mere });
    this.usluge.at(currentIndex).patchValue({ stopaPdv: this.sveUsluge[this.indexOfUsluga].pdv });
    this.usluge.at(currentIndex).patchValue({ kolicina: '' });
    this.usluge.at(currentIndex).patchValue({ popust: '' });
  }

  getKolicina(kolicina: any, pointIndex: any) {
    const currentIndex = this.currentFillingIndex;
    this.currentKolicina = kolicina;
    this.getPopust(this.currentPopust, pointIndex);
  }

  getPopust(popust: any, pointIndex: any) {
    if (popust === "") {
      popust = "0";
    }
    this.currentPopust = popust;
    this.currentOsnovica =
      this.currentPopust != undefined ? this.currentKolicina * this.sveUsluge[this.indexOfUsluga].cena_po_jedinici_mere - (this.currentPopust / 100) * (this.currentKolicina * this.sveUsluge[this.indexOfUsluga].cena_po_jedinici_mere) : 0;
    this.usluge.at(pointIndex).patchValue({
      osnovica: this.currentOsnovica,
      pdv: this.currentPopust != undefined ? this.currentOsnovica * this.sveUsluge[this.indexOfUsluga].pdv / 100 : 0,
      ukupno: this.currentPopust != undefined ? this.currentOsnovica + this.currentOsnovica * this.sveUsluge[this.indexOfUsluga].pdv / 100 : 0,
      naziv: this.sveUsluge[this.indexOfUsluga].naziv
    });

  }

  updatePdv(index: any) {
    this.currentUkupanPdv = 0;
    this.usluge.controls.forEach((el, index) => {

      this.currentUkupanPdv += el.value.pdv;

    });
    this.klijentForm.controls['ukPdv'].setValue(this.currentUkupanPdv);
  }

  updateOsnovica(index: any) {
    this.currentUkupnaOsnovica = 0;
    this.usluge.controls.forEach((el, index) => {

      this.currentUkupnaOsnovica += el.value.osnovica;

    });
    this.klijentForm.controls['ukOsnovica'].setValue(this.currentUkupnaOsnovica);
  }

  updateUkupno(index: any) {
    this.currentUkupno = 0;
    this.usluge.controls.forEach((el, index) => {

      this.currentUkupno += el.value.ukupno;

    });
    this.klijentForm.controls['ukZaUplatu'].setValue(this.currentUkupno);
  }

  get usluge() {
    return this.klijentForm.get('usluge') as FormArray;
  }

  addUsluge() {

    this.usluge.push(this.fb.group({
      naziv: '',
      jm: '',
      cenaJm: '',
      kolicina: '',
      popust: '',
      osnovica: '',
      stopaPdv: '',
      pdv: '',
      ukupno: ''
    }));
  }

  deleteUsluga(index: number) {
    this.currentFillingIndex--;
    this.usluge.removeAt(index);
  }

  initNaslov(): void {

    if (this.tipRacuna == "kr") {

      this.naslovRacuna = "Račun";
    } else if (this.tipRacuna == "ar") {

      this.naslovRacuna = "Avansni račun";
    } else if (this.tipRacuna == "pr") {

      this.naslovRacuna = "Predračun";
    } else {
      this.router.navigate(['404']);
    }
  }

  racunPlacen(): void {
    // console.log(this.racunId);
    this.http.patch(API_URL + 'predracuni/placen', {racunId: this.racunId}).subscribe(x => {
      console.log(x);
      this.router.navigate(['racuni/kr'])
    });
  }

}


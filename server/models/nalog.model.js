const mongoose = require('mongoose');
const { nalog } = require('.');

const nalogSchema = mongoose.Schema({
    login_email: { type: String, required: true, match: /\S+@\S+\.\S+/ },
    password: { type: String, required: true },
    naziv: { type: String, required: true },
    maticni_broj: { type: String, minLength: 8, maxLength: 8 },
    pib: { type: String, required: true, minLength: 8, maxLength: 8 },
    telefon: { type: String },
    email: { type: String, match: /\S+@\S+\.\S+/ },
    website: String,
    adresa: { type: String, required: true },
    postanski_broj: { type: String, required: true, minLength: 5, maxLength: 5 },
    grad: { type: String, required: true },
    tip_identifikacije: String,
    pdv_obveznik: { type: Boolean, default: false },
    max_predracun_broj: { type: Number, default: 0 },
    max_racun_broj: { type: Number, default: 0 },
    logoUrl: {type: String, default: '../../img/logo-placeholder.png'},
    bankovni_racuni: {
        type: [{
            broj_racuna: { type: String, required: true },
            valuta: { type: String, required: true }
        }],
        required: false
    },
    klijenti: {
        type: [{
            naziv: { type: String, required: true },
            maticni_broj: { type: String, minLength: 8, maxLength: 8 },
            pib: { type: String, required: true, minLength: 8, maxLength: 8 },
            ime_zastupnika: { type: String },
            adresa: { type: String, required: true },
            postanski_broj: { type: String, required: true, minLength: 5, maxLength: 5 },
            grad: { type: String, required: true },
            zemlja: { type: String, required: true },
            telefon: { type: String },
            email: { type: String, match: /\S+@\S+\.\S+/ }
        }],
        required: false
    },
    proizvod_usluga: {
        type: [{
            naziv: { type: String, required: true },
            opis: String,
            cena_po_jedinici_mere: { type: Number, required: true },
            jedinica_mere: { type: String, required: true },
            pdv: { type: Number, required: true }
        }],
        required: false
    }
});

// poslednji atribut je naziv kolekcije
// po konvenciji se naziva u mnozini
// ako se ne navede naziv, mongoose dodaje 's' na kraj imena modela
const nalogModel = mongoose.model('Nalog', nalogSchema, 'nalozi');
module.exports = nalogModel;

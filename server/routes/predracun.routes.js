const { authJwt, predracunMiddleware, nalogMiddleware } = require("../middlewares");
const controller = require("../controllers/predracun.controller");
var multer = require('multer');

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/predracun/info", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.predracuniInfo);
  app.patch("/api/predracun/izdaj", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.izdajPostojeciPredracun);
  app.patch("/api/racun/izdaj", authJwt.verifyToken, nalogMiddleware.getNalog, controller.izdajRacunDirektno);
  app.patch("/api/predracun/izdaj-racun", authJwt.verifyToken, predracunMiddleware.getPredracuni, nalogMiddleware.getNalog, controller.izdajRacunIzPredracuna);
  app.delete("/api/predracun/:id", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.obrisiPredracun);
  app.patch("/api/predracun", authJwt.verifyToken, nalogMiddleware.getNalog, controller.dodajPredracun);
  app.get("/api/predracuni/:id", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.getPredracunById);
  app.get("/api/predracuni", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.getPredracuni);
  app.get("/api/racuni", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.getRacuni);
  app.post("/api/predracuni/send-email", controller.sendEmail);
  app.patch("/api/predracuni/placen", authJwt.verifyToken, predracunMiddleware.getPredracuni, controller.racunPlacen);
};

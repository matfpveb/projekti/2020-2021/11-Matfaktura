import { Component, OnInit } from "@angular/core";
import { AuthService } from "../_services/auth.service";
import {
  FormGroup,
  FormControl,
  ValidationErrors,
  Validators,
  AbstractControl
} from "@angular/forms";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

const API_URL = "http://localhost:8080/api/";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  form: any = {
    login_email: null,
    password: null,
    naziv_firme: null,
    maticni_broj: null,
    pib: null,
    telefon: null,
    kontakt_email: null,
    website: null,
    adresa: null,
    grad: null,
    postanski_broj: null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = "";

  registerForm: FormGroup;

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private router: Router
  ) {
    this.registerForm = new FormGroup({
      login_email: new FormControl("", [
        Validators.required,
        Validators.pattern(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/),
        Validators.minLength(3),
        Validators.maxLength(50)
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      naziv_firme: new FormControl("", [Validators.required]),
      maticni_broj: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[12569][0-9]{7}$/)
      ]),
      pib: new FormControl("", [
        Validators.required,
        Validators.pattern(/^(?!10000000)[1-9][0-9]{7}$/)
      ]),
      telefon: new FormControl("", [
        Validators.required,
        Validators.pattern(/^0[1-9][0-9]{7,8}$/)
      ]),
      kontakt_email: new FormControl("", [
        Validators.pattern(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/)
      ]),
      website: new FormControl("", [
        Validators.required,
        Validators.pattern(/^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/)
      ]),
      adresa: new FormControl("", [Validators.required]),
      grad: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/)
      ]),
      postanski_broj: new FormControl("", [
        Validators.required,
        Validators.pattern(/^[0-9]{5}$/)
      ])
    });
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(["dashboard"]);
    }
  }

  sendEmail() {
    let klijent = {
      ime_klijenta: this.registerForm.get("naziv_firme")?.value,
      email: this.registerForm.get("login_email")?.value
    };
    
    this.http
      .post(API_URL + "predracuni/send-email", { klijent: klijent })
      .subscribe();
  }

  public loginEmailHasError(): string {
    return this.fieldHasErrors("login_email") ? "alert-danger" : "";
  }

  public passwordHasError(): string {
    return this.fieldHasErrors("password") ? "alert-danger" : "";
  }

  public nazivFirmeHasError(): string {
    return this.fieldHasErrors("naziv_firme") ? "alert-danger" : "";
  }

  public maticniBrojHasError(): string {
    return this.fieldHasErrors("maticni_broj") ? "alert-danger" : "";
  }

  public pibHasError(): string {
    return this.fieldHasErrors("pib") ? "alert-danger" : "";
  }

  public telefonHasError(): string {
    return this.fieldHasErrors("telefon") ? "alert-danger" : "";
  }

  public kontaktEmailHasError(): string {
    return this.fieldHasErrors("kontakt_email") ? "alert-danger" : "";
  }

  public websiteHasError(): string {
    return this.fieldHasErrors("website") ? "alert-danger" : "";
  }

  public adresaHasError(): string {
    return this.fieldHasErrors("adresa") ? "alert-danger" : "";
  }

  public gradHasError(): string {
    return this.fieldHasErrors("grad") ? "alert-danger" : "";
  }

  public postanskiBrojHasError(): string {
    return this.fieldHasErrors("postanski_broj") ? "alert-danger" : "";
  }

  loginEmailErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get(
      "login_email"
    );
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Login e-mail je obavezan.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Login e-mail mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
          `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (errors.maxlength) {
      errorMessages.push(
        `Login e-mail može da sadrži najviše  ${errors.maxlength.requiredLength} karaktera. ` +
          `Uneli ste ${errors.maxlength.actualLength} karaktera.`
      );
    }

    if (errors.pattern) {
      errorMessages.push(
        "Login e-mail je pogrešnog formata." +
          "E-mail je oblika primer@email.com"
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  passwordErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get("password");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Lozinka je obavezna.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Lozinka mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
          `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  nazivFirmeErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get(
      "naziv_firme"
    );
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Naziv firme je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  maticniBrojErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get(
      "maticni_broj"
    );

    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Matični broj je obavezan.");
    }

    if (errors.pattern) {
      errorMessages.push(
        "Matični broj je pogrešnog formata." +
          "Matični broj počinje sa 1, 2, 5, 6 ili 9 i sastoji se od 8 cifara."
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  pibErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get("pib");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Pib je obavezan.");
    }

    if (errors.pattern) {
      errorMessages.push(
        "Pib je pogrešnog formata. Pib je broj između 10000001 i 99999999"
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  telefonErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get("telefon");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Telefon je obavezan.");
    }

    if (errors.pattern) {
      errorMessages.push(
        "Telefon je pogrešnog formata." +
          "Telefon počinje sa 0 i sastoji se od 9 ili 10 cifara."
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  kontaktEmailErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get(
      "kontakt_email"
    );
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.pattern) {
      errorMessages.push("Kontakt e-mail je pogrešnog formata." + "E-mail je oblika primer@email.com");
    }

    return errorMessages;
  }

  websiteErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get("website");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Website je obavezan.");
    }

    if (errors.pattern) {
      errorMessages.push("Website je pogrešnog formata."+ "Website je oblika www.primer.com");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  adresaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get("adresa");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Adresa je obavezna.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  gradErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get("grad");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Grad je obavezan.");
    }

    if(errors.pattern){
      errorMessages.push("Grad je pogrešnog formata.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  postanskiBrojErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.registerForm.get(
      "postanski_broj"
    );
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Poštanski broj je obavezan.");
    }

    if(errors.pattern){
      errorMessages.push("Poštanski broj je pogrešnog formata." + "Poštanski broj se sastoji od 5 cifara.")
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  private fieldHasErrors(fieldName: string): boolean {
    const control: AbstractControl | null = this.registerForm.get(fieldName);
    if (control === null) {
      return false;
    }
    const errors: ValidationErrors | null = control.errors;
    return errors != null;
  }

  onSubmit(): void {
    if (this.registerForm.invalid) {
      window.alert("Nevalidna forma!");
      return;
    }

    const {
      login_email,
      password,
      naziv_firme,
      maticni_broj,
      pib,
      telefon,
      kontakt_email,
      website,
      adresa,
      grad,
      postanski_broj
    } = this.registerForm.value;

    let kont_email;

    if (kontakt_email === "") {
      kont_email = login_email;
    } else {
      kont_email = kontakt_email;
    }

    this.authService
      .register(
        login_email,
        password,
        naziv_firme,
        maticni_broj,
        pib,
        telefon,
        kont_email,
        website,
        adresa,
        grad,
        postanski_broj
      )
      .subscribe(
        data => {
          
          this.isSuccessful = true;
          this.isSignUpFailed = false;
          this.sendEmail();
          this.reloadPage();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSignUpFailed = true;
        }
      );
  }

  reloadPage(): void {
    window.location.href = "/login";
  }
}

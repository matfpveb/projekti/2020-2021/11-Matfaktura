exports.nalogInfo = (req, res) => {
    var nalog_info = {
        login_email: req.nalog.login_email,
        naziv: req.nalog.naziv,
        maticni_broj: req.nalog.maticni_broj,
        pib: req.nalog.pib,
        telefon: req.nalog.telefon,
        email: req.nalog.email,
        website: req.nalog.website,
        adresa: req.nalog.adresa,
        postanski_broj: req.nalog.postanski_broj,
        grad: req.nalog.grad,
        tip_identifikacije: req.nalog.tip_identifikacije,
        pdv_obveznik: req.nalog.pdv_obveznik,
        broj_klijenata: req.nalog.klijenti.length,
        klijenti: req.nalog.klijenti,
        logoUrl: req.nalog.logoUrl
        
    }
    res.status(200).json(nalog_info);
};

exports.nalogUpdate = async (req, res) => {
    var nalog = req.nalog;
    try {
        nalog.naziv = req.body.naziv;
        nalog.adresa = req.body.adresa;
        nalog.postanski_broj = req.body.postanski_broj;
        nalog.telefon = req.body.telefon;
        nalog.grad = req.body.grad;
        nalog.email = req.body.email;
        nalog.website = req.body.website;
        await nalog.save();
        res.status(200).json({ message: "Ušpesno ažuriran nalog" });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

exports.getPDV = (req, res) => {
    var nalog = req.nalog;
    res.status(200).json(nalog.pdv_obveznik);
}

exports.updatePDV = async (req, res) => {
    var nalog = req.nalog;
    try {
        nalog.pdv_obveznik = req.body.pdv_obveznik;
        await nalog.save();
        res.status(200).json({ message: "Ušpesno ažuriran status PDV obveznika" });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

exports.dodajUslugu = async (req, res) => {
    var nalog = req.nalog;
    nalog.proizvod_usluga.pull({ _id: req.body._id });

    nalog.proizvod_usluga.push(req.body);
    await nalog.save();
    res.status(200).json({ message: "Uspesno dodata usluga." })
}

exports.uslugeInfo = (req, res) => {
    res.status(200).json(req.nalog.proizvod_usluga);
}

exports.getUslugaById = (req, res) => {
    const uslugaId = req.params.id;
    const usluge = req.nalog.proizvod_usluga;
    var usluga = null;
    if (usluga = usluge.find(x => x._id == uslugaId)) {
        res.status(200).json(usluga);
    } else {
        res.status(404).send({ message: "Usluga sa prosleđenim id nije pronađena!" })
    }
}

exports.dodajKlijenta = async (req, res) => {
    var nalog = req.nalog;
    nalog.klijenti.pull({ _id: req.body._id });
    nalog.klijenti.push(req.body);
    await nalog.save();
    res.status(200).json({ message: "Uspesno dodat klijent." })
}

exports.klijentiInfo = (req, res) => {
    res.status(200).json(req.nalog.klijenti);
}


exports.getKlijentById = (req, res) => {
    const klijentId = req.params.id;
    const klijenti = req.nalog.klijenti;
    var klijent = null;
    if (klijent = klijenti.find(x => x._id == klijentId)) {
        res.status(200).json(klijent);
    } else {
        res.status(404).send({ message: "Klijent sa prosleđenim id nije pronađen!" })
    }
}

exports.deleteKlijent = async (req, res) => {
    await req.nalog.klijenti.pull({ _id: req.body.klijentId });
    await req.nalog.save();
    res.status(200).json({ message: "Uspesno obrisan klijent" });
}

exports.deleteUsluga = async (req, res) => {
    await req.nalog.proizvod_usluga.pull({ _id: req.body.inventoryId });
    await req.nalog.save();
    res.status(200).json({ message: "Uspesno obrisana usluga" });

}

exports.getBankovniRacuni = (req, res) => {
    res.status(200).json(req.nalog.bankovni_racuni);
}

exports.addBankovniRacun = async (req, res) => {
    var nalog = req.nalog;
    nalog.bankovni_racuni.pull({ _id: req.body._id });
    nalog.bankovni_racuni.push(req.body);
    await nalog.save();
    res.status(200).json({ message: "Uspesno dodat bankovni racun." })
}

exports.deleteBankovniRacun = async (req, res) => {
    await req.nalog.bankovni_racuni.pull({ _id: req.body._id });
    await req.nalog.save();
    res.status(200).json({ message: "Uspesno obrisan bankovni racun" });
}

exports.uploadLogo = async (req, res) => {   
    
    const logoUrl = req.file.filename;
    var nalog = req.nalog;
    nalog.logoUrl= logoUrl;
    
    
    await nalog.save();

    res.status(200).json({message: "Uspesno uploadovan logo."});
    
}



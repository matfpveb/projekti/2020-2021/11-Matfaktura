const db = require('../models/');
const Predracun = db.predracun;

getPredracuni = async (req, res, next) => {
    let predracuni;
    try {
        predracuni = await Predracun.find({ nalog_id: req.userId });
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    req.predracuni = predracuni;
    
    next();
}


const predracunMiddleware = {
    getPredracuni
};

module.exports = predracunMiddleware;
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NalogInfoComponent } from './nalog-info.component';

describe('NalogInfoComponent', () => {
  let component: NalogInfoComponent;
  let fixture: ComponentFixture<NalogInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NalogInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NalogInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

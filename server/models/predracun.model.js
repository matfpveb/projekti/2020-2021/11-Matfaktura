const mongoose = require('mongoose');

const predracunSchema = mongoose.Schema({
    broj: {type: String, required: true},
    nalog_id: {type: mongoose.Schema.Types.ObjectId, required: true},
    nalog_info: {
        naziv: {type: String, required: true},
        maticni_broj: String,
        pib: {type: String, required: true},
        telefon: String,
        email: String,
        website: String,
        adresa: {type: String, required: true},
        postanski_broj: {type: String, required: true},
        grad: {type: String, required: true},
        tip_identifikacije: String,
        pdv_obveznik: Boolean,
        bankovni_racuni: {
            broj_racuna: String,
            valuta: String
        }
    },
    klijent: {
        naziv: {type: String, required: true},
        maticni_broj: String,
        pib: {type: String, required: true},
        ime_zastupnika: String,
        adresa: {type: String, required: true},
        postanski_broj: {type: String, required: true},
        grad: {type: String, required: true},
        zemlja: {type: String, required: true},
        telefon: String,
        email: String
    },
    datum_izdavanja: String,
    mesto_izdavanja: String,
    rok_placanja: String,
    napomena: String,
    izdat: {type: Boolean, default: false},
    cena: Number,
    lista_usluga:[{
        proizvod_usluga_info:{
            naziv: {type: String, required: true},
            opis: String,
            cena_po_jedinici_mere: {type: Number, required: true},
            jedinica_mere: {type: String, required: true},
            pdv: {type: Number, required: true}
        },
        kolicina: {type: Number, required: true},
        popust: {type: Number, default: 0.0}
    }],
    avansni_racuni:{
        type: [{
            broj: {type: String, required: true},
            mesto_izdavanja: {type: String, required: true},
            datum_izdavanja: {type: Date, default: Date.now},
            iznos: {type: Number, required: true},
            pdv: {type: Number, required: true},
            izdat: {type: Boolean, default: false },
            zatvoren: {type: Boolean, default: false }
        }],
        required: false
    },
    racun:{
        type:{
            broj: {type: String, required: true},
            mesto_izdavanja: {type: String, required: true},
            datum_izdavanja: {type: String, required: true },
            mesto_prometa: String,
            datum_prometa: String,
            rok_placanja: {type: String, required: true },
            placeno: {type: Boolean, required: true, default: false},
            ukupno_za_placanje: {type: mongoose.Number, required: true},
            napomena: String,
        },
        required: false
    }
});

predracunSchema.methods.calculateCena = function() {
    var ukupno = 0;
    this.lista_usluga.forEach(element => {
        var cena = element.proizvod_usluga_info.cena_po_jedinici_mere;
        cena *= element.kolicina;
        cena *= (100 + element.proizvod_usluga_info.pdv);
        cena *= (100 - element.popust);
        cena /= 10000;
        ukupno += cena;
    });
    return ukupno;
}
const predracunModel = mongoose.model('Predracun', predracunSchema, 'predracuni');
module.exports = predracunModel;

# Prokat Matfaktura

Aplikaciju za izdavanje faktura, racuna, predracuna.

## Developeri

- [Ikovic Svetozar, 69/2016](https://gitlab.com/toswe)
- [Dimitrije Stamenić, 260/2016](https://gitlab.com/stamd)
- [Mihajlo Zivkovic, 87/2016](https://gitlab.com/mi16087)
- [Lazar Ristić, 150/2016](https://gitlab.com/lristic)
- [Dimitrije Petrović, 466/2017](https://gitlab.com/mitokv)

## Pre Inicijalizacije projekta

Prvo je potrebno imati instalirane [node.js and npm](https://www.geeksforgeeks.org/installation-of-node-js-on-linux/) i [mongodb](https://docs.mongodb.com/manual/administration/install-community/) da bi aplikacija mogla da se pokrene.
Zatim instalirat sledeće pakete:
```shell
npm install -g @angular/cli
```

## Inicijalizacija Projekta

Klonirati repozitorijum i pozicionirati se u klonirani direkorijum i instalirati odgovarajuće pakete:
```shell
git clone git@gitlab.com:matfpveb/projekti/2020-2021/11-Matfaktura.git matfaktura
cd 11-Matfaktura
npm install
```

pozicionirati se u server direkorijum kloniranog direktorijuon i instalirati node pakete:
```shell
cd server
npm install
```

pozicionirati se u angular direkorijum kloniranog direktorijuon i instalirati angular pakete:
```shell
cd angular
npm install
```

## Pokretanje projekta
### Pokretanje Servera

Pozicionirati se u server direktorijum i pokrenuti komandu:
```shell
nodemon server.js
```

### Pokretanje Aplikacije

Pozicionirati se u angular direktorijum i pokrenuti komandu:
```shell
ng serve --open
```

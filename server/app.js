const express = require('express');
const { urlencoded, json } = require('body-parser');

const nalogRoutes = require('./routes/nalog');
const indexRouter = require('./routes/index');
const loginRouter = require('./routes/login');
const registerRouter = require('./routes/register');


const app = express();
app.use(json());
app.use(urlencoded({ extended: false }));


app.use('/assets', express.static('assets'));
app.use('/', indexRouter);
app.use('/nalog', nalogRoutes);
app.use('/login', loginRouter);
app.use('/register', registerRouter);

module.exports = app;

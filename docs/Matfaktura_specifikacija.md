# Matfaktura - specifikacija projekta

## Sadržaj

[TOC]

## Definisanje pojmova

### Račun / Faktura

Računovodstvena isprava koja se se sastavlja i dostavlja preduzetnicima i pravnim licima u elektronskom obliku. To je **osnovni dokument koji svedoči o izvršenom prometu dobara ili usluga**, njime "tvrdimo da smo kupcu isporučili robu ili uslugu".

Mora biti **potvrđena od strane odgovornog lica** koje svojim potpisom (ili drugom identifikacionom oznakom koja je utvrđena opštim aktom preduzeća) potvrđuje njenu verodostojnost.

#### Identifikacione oznake

- Ime i prezime odgovornog lica
- Faksimil
- Elektronski potpis
- Druga oznaka ili kombinacija (npr. ime, prezime i potpis )

Račun **mora biti izdat na dan prometa ili neposredno nakon vršenja prometa** (nikako pre prometa, tada se smatra nevalidnim). Izdavanje se vrši onog momenta kada se isporuči roba, stavi na raspolaganje kupcu u magacinu ili se izvrši ugovorena usluga (izrada aplikacije, dostavljanje i instalacija telekomunikacione orpreme).

Postoji i opcija **periodičnog fakturisanja** na duže ugovore (mesečno, kvartalno...). Ova opcija je obavezna za usluge koje se izvršavaju duže od godinu dana, jer se sama faktura ne sme izdati na period duži od godinu dana.

#### Obavezni elementi računa

1. naziv, adresu i PIB obveznika - izdavaoca računa;
2. mesto i datum izdavanja i redni broj računa;
3. naziv, adresu i PIB obveznika - primaoca računa;
4. vrstu i količinu isporučenih dobara ili vrstu i obim usluga;
5. datum prometa dobara i usluga i visinu avansnih plaćanja;
6. iznos osnovice;
7. poresku stopu koja se primenjuje;
8. iznos PDV koji je obračunat na osnovicu;
9. napomenu o odredbi ovog zakona na osnovu koje nije obračunat PDV;
10. napomenu da se za promet dobara i usluga primenjuje sistem naplate.

#### Korisni linkovi

- [MojeFakture - Račun blog](https://blog.mojefakture.com/racun-faktura/)
- [MojeFakture - Kada se izdaje račun](https://blog.mojefakture.com/izdavanje-racuna-kada-se-racun-izdaje/)

### Avansni račun

Račun za **uplatu koja je izvršena pre isporuke dobara ili izvršene usluge**. Uplata može biti inicijalizovana predračunom ili pozivom za plaćanje, ali i ne mora biti, već je dovoljno da se avansno plaćanje definiše u ugovoru.

Avansni račun izdaju **samo obaveznici PDV-a** koji su u toku određenog poreskog preioda (mesec ili tromesečje) primili uplatu za dobra ili usuge koje neće biti isporučene u tom istom poreskom periodu, tj **promet dobara/usluga neće biti izvršen u istom poreskom periodu u kom je primljena avansna uplata.**

Svaka avansna uplata **sadrži u sebi PDV**, pa se pri njenom registrovanju na osnovu iznosa računa PDV koji se iskazuje u avansnom računu.

```
Primer obračuna PDV-a na osnovu avansne uplate od 30.000,00 RSD: 
---------------------------------------------------------------
Iznos uplate: 	30.000,00 RSD
Stopa PDV-a: 	20%
Osnovica PDV-a:	30.000,00 / 1.2 = 25.000,00 RSD
Iznos PDV-a:	5.000,00 RSD
```

Kada se izvrši promet dobara, odnosno izvrpi usluga koja je predmet avansnog računa, **izdaje se konačni račun** koji sadrži informacije o avansnim uplatama, kao i o PDV-u iskazanom u avansnom računu.

#### Konačni račun

**Nakon isporuke dobara i usluga za koju je primljena avansa uplata** i izdat avansni račun po kom je prodavac platio PDV, prodavac će **izdati račun u kojem će prikazati iznose avansnih uplata i PDV obračunat iz njih**. Na (konačnom) računu, prodavac će od ukupnog zaduženja oduzeti avansne uplate i porez sadržan u njima i prikazati razliku koju je kupac dužan da doplati odnosno razliku poreske obaveze koju je dužan da prikažu u poreskoj prijavi.

```
Primer obračuna na konačnom računu za isporučenu robu u vrednosti od 150.000,00 RSD + PDV, sa tri avansne uplate od po 30.000,00 RSD, 20.000,00 RSD i 80.000,00 RSD
-------------------------------------------------------------------------------------
Osnovica za PDV:	150.000,00 RSD
PDV 20%:			 30.000,00 RSD
UKUPNO SA PDV:		180.000,00 RSD
--------------
Avans 1:			 30.000,00 RSD
PDV iz Avans 1:		  6.000,00 RSD
Avans 2:			 20.000,00 RSD
PDV iz Avans 2:		  3.333,33 RSD
Avans 3:			 80.000,00 RSD
PDV iz Avans 3:		 13.333,33 RSD
--------------
RAZLIKA ZA UPLATU:	 50.000,00 RSD
```

#### Obavezni elementi avansnog računa

1.  naziv, adresu i PIB obveznika – izdavaoca računa;
2. mesto i datum izdavanja i redni broj računa (datum uplate avansa);
3. naziv, adresu i PIB obveznika – primaoca računa;
4. osnov za avansno plaćanje;
5. iznos avansne uplate;
6. poresku stopu koja se primenjuje;
7. iznos obračunatog PDV.

#### Korisni linkovi

- [LEPO OBJAŠNJENO - ŠTA JE AVANSNI RAČUN SA PRIMERIMA](http://www.svezor.rs/sta-je-to-avansni-racun-i-u-cemu-se-razlike-od-predracuna)
- [MojeFakture YT Video Avansni račun](https://www.youtube.com/watch?v=VjZ0By_SKUI&ab_channel=MojeFakture)
- [Avansni račun i PDV obaveza](https://www.overa.rs/avansni-racun-i-pdv-obaveza-osnov-i-vreme-izdavanja-racuna.html)
- [MojeFakture - Avansni i konačni račun](https://www.mojefakture.rs/program-za-fakturisanje/avansni-konacni-racun)
- [MojeFakture - Avansni račun blog](https://blog.mojefakture.com/avansni-racun/)

### Predračun

Dokument kojim jedan privredni subjekat daje cenovnu ponudu za određenu robu ili uslugu drugom privrednom subjektu - potencijalnom kupcu. On se u suštini može posmatrati kao **poziv na plaćanje,** jer osim ponude sadrži i instrukcije za plaćanje.

Za razliku od fakture (računa), predračun ne obavezuje svog izdavaoca da plati PDV. Kada privredni subjekat koji je primio predračun izvši plaćanje na osnovu istog, tada mu se izdaje račun (faktura), na osnovu koje se vrši knjiženje i stvara se obaveza plaćanja PDV-a.

#### Korisni linkovi

- [MojeFakture - Predračun blog](https://blog.mojefakture.com/profaktura-predracun-primer/)

## Vizuelni identitet aplikacije



## Baza podataka

Trenutna arhitektura baze podataka:

![2021_04_11_matfaktura_baza](img/matfaktura_baza.png)

## Definisanje funkcionalnosti

Ovde ćemo definisati skup funkcionalnosti programa **matfaktura** koji će biti na raspolaganju korisniku kada se uloguje u kontrolnu tablu programa.

### Početna strana

Na početnoj strani se nalazi obračun stanja u prethodnih 12 meseci - pregled iznosa izdatih računa, iznos obračunatih poreza, iznosi naplaćenih računa itd. 

U donjem desnom uglu imamo plutajuće `+ dugme` (kao u npr gmail aplikaciji). Klikom na ovo dugme nam iskače oblačić koji nam daje mogućnosti da dodamo:

- novu stavku u inventaru
- novog klijenta
- novi predračun
- novi avansni račun
- novi račun

### Inventar (proizvodi i usluge)

Korisnik ima mogućnost da kreira svoj inventar kako ne bi morao da ručno ukucava svoje proizvode i usluge na svaki račun posebno. Ova funkcionalnos omogućava automatsko popunjavanje specifikacije računa sa već postojećim stavkama iz inventara.

U odeljku inventar vidimo:

- tabelu sa svim do sada dodatim stavkama
  - svaka stavka ima mogućnost da se izmeni ili obriše
  - postoji mogućnost masovnijeg selektovanja više stavki, na koje bi se primenila neka od akcija (npr. brisanje)
- dugme **dodaj novu stavku**

#### Forma za dodavanje i izmenu stavki iz inventara

Ovoj formi pristupamo ili klikom na dugme dodaj novu stavku ili odabirom opcije da izmenimo postojeću stavku. Njom uređujemo unose u tabelu `Proizvod_usluga` iz baze podataka:

- **pu_id** - [INT] jedinstveni identifikacioni broj artikla, jedinstven na nivou jednog naloga  
- **n_id** - [INT] strani ključ kojim se vezujemo za trenutni nalog
- **pu_naziv** - [VARCHAR] naziv artikla
- **pu_opis** - [TEXT] detaljniji opis artikla
- **pu_jm** - [VARCHAR] jedinica mere (KOM, KG, MESEC itd)
- **pu_cena_jm** - [DECIMAL] cena po jedinici mere
- **pu_pdv** - [DECIMAL(4,2)] stopa PDV-a, dozvoljene vrednosti su 20, 10 i 0

### Baza klijenata

Korisnik ima mogućnost da kreira svoju bazu klijenata sa kojima je do sada već sarađivao kako ne bi morao da ručno ukucava sve podatke o klijentima na svaki račun posebno. Ova funkcionalnos omogućava automatsko popunjavanje računa sa već postojećim informacijama o klijentima.

U odeljku klijenti vidimo:

- tabelu sa svim postojećim klijentima
  - mogućnost izmena i brisanja klijenta (ili jednostavnog pristupa profilu klijenta)
  - mogućnost sortiranja po određenim kriterijumima
  - za svakakog klijenta potencijalno vidimu ukupnu cifru koju treba da nam plati, koliko nam je do sada platio itd.
  - postoji mogućnost masovnijeg selektovanja više stavki, na koje bi se primenila neka od akcija (npr. brisanje)
- dugme dodaj novog klijenta

#### Profil klijenta

Profil klijenta vidimo kada odaberemo opciju da izmenimo informacije o postojećem klijentu ili da napravimo novog. Na profilu samog klijenta imamo pregled relevantnih informacija iz tabele `Klijent`:

- **k_id**
- **k_naziv**
- **k_mb** 
- **k_pib**
- **k_ime_zastupnika**
- **k_adresa**
- **k_postanski_broj**
- **k_grad**
- **k_zemlja**
- **k_telefon**
- **k_email**

Pored izmena unosa iz baze podataka, imamo i **pregled svih tipova računa koji su izdati za datog klijenta**. Imamo tri taba: `računi`, `predračuni` i `avansni računi`. Svaki od tabova sadrži  tabelu sa svim odgovarajućim računima za datog klijenta, sadržaj tih tabela dobijamo na sledeći način:

```sql
-- Predracun
SELECT *
FROM predracun 
WHERE   n_id = (id_trenutnog_naloga)
    AND k_id = (id_trenutnog_klijenta)
    
-- Avansni racun
SELECT pr.k_id, a.*
FROM predracun AS pr, avansni_racun AS a
WHERE a.pr_id = pr.pr_id
    AND pr.k_id = (id_trenutnog_klijenta)
    
-- (Konacni) racun
SELECT pr.k_id, r.*
FROM predracun AS pr, racun AS r
WHERE r.pr_id = pr.pr_id
    AND pr.k_id = (id_trenutnog_klijenta)
```

> **//TODO:** Treba još definisati koje kolone da se prikazuju za svaki red u ovim tabelama na profilu klijenta

Takođe možemo imati i neku **grafiku koja prikazuje istoriju naše saradnje sa ovim klijentom**, npr. kolika je ukupna vrednost svih izdatih računa ovom klijentui, koliko je ukupno on platio, koliko je još neplaćeno itd.

### Predračuni

Ideja je da se pre svakog obavljenog posla, kada je dogovor pao, u bazu unese makar *draft* predračuna. Taj unos *interno* predstavlja svedočenje o započetom poslu.

Na ovoj stranici korisnik vidi tabelu sa svim napravljenim predračunima. Svaki red ima:

- redni broj predračuna - kada se on klikne otvara se forma za izmenu predračuna
- oznaku da li je predračun izdat ili je i dalje u draft stanju
- dugme za konverziju u račun
- dugme za slanje na mejl klijentu
- ...

**Podrazumevano stanje** je da predračun *nije izdat* nego samo služi kao pivot tabela preko koje bi tabele `Racun` i `Avansni_racun` znale informacije o dogovorenom poslu, odnosno koje smo sve proizvode i usluge i u kojoj količini dogovorili da isporučimo nekom klijentu. 

> **// TODO:** U tom slučaju, ako dođe do situacije da postoji potreba da se **izdaje avansni račun** za neku primljenu uplatu, šta raditi

Ukoliko postoji potreba da se **predračun izda**, postoji opcija da popunimo datum izdavanja predračuna u tabeli `Predracun` i tada se smatra da je isti **izdat**, tako da nam se otvara opcija da ga klijentu pošaljemo na njegovu email adresu.

Takođe postoji i opcija da se **predračun pretvori u račun** kada se izvrši promet dobara. Tada se pravi novi zapis u tabeli  `Racun` sa novim mestima i datumima izdavanja i prometa. 

Za jedan predračun može postojati **samo jedan račun**, ali i **više avansnih računa.** 

```sql
-- Informacije o KLIJENTU 
SELECT *
FROM Klijent
WHERE k_id = (odabrani_id_klijenta)

-- Informacije o IZDAVAOCU 
SELECT n_naziv, n_adresa, n_postanski_broj, n_grad, n_zemlja, n_pib, n_mb, n_telefon, n_email, n_website
FROM Nalog
WHERE n_id = (id_trenutnog_naloga)

-- Informacije o SPECIFIKACIJI USLUGA I DOBARA
SELECT lu.*, pu.*
FROM Lista_usluga AS lu, Proizvod_usluga AS pu
WHERE 	lu.pr_id = (id_trenutnog_predracuna)
		AND lu.pu_id = pu.pu_id
```

Kada unesemo podatke o klijentu trebalo bi da imamo opciju da sačuvamo te podatke u bazu, zarad potencijalno ponovnog korišćenja u budućim računima.

Na pregledu samog predračuna (forma za pravljenje i ispravku) imamo i evidenciju da li ima avansnih uplata po tom konkretnom predračunu, te informacije su bitne zbog kasnijeg izdavanja konačnog računa. 

### Avansni računi

Avansni račun izdaju obveznici PDV-a u slučaju kada je neka uplata primljena za posao koji neće biti završen u tekućem poreskom periodu. O tome sami korisnici treba da vode računa, mi se ne brinemo za to, mi samo dajemo mogućnost izdavanja.

> **Pažnja: ** Ovaj odeljak se prikazuje samo korisnicima koji su u sistemu PDV-a!

Ova stranica sadrži tabelu sa svim do sada izdatim avansnim računima i dugme kojim pravimo novi avansni račun. Tabela je podeljenja na sekcije za izdate račune i draftove. A u svakom redu je označeno da li je taj avansni račun i dalje otvoren ili je zatvoren izdavanjem konačnog računa. (Ovo može biti i obrnuto, da imamo podelu tabele na otvorene i zatvorene račune, a da pored svakog računa piše da li je draft ili je izdat).

Imamo mogućnost da odaberemo više avansnih računa i da kliknemo na dugme **kreiraj konačni račun**, i da tako kreiramo konačni račun na osnovu više avansnih uplata. Svi avansni računi su povezani (makar) sa nekim draft predračunom, tako da na osnovu toga imamo specifikaciju konačnog računa, a na osnovu odabranih avansnih računa imamo specifikaciju izvršenih avansnih uplata.

Za razliku od predračuna, koga u principu možemo bez ograničenja menjati, avansni račun se ne bi smeo menjati posle izdavnja. Zato bi bilo dobro da omogućimo pravljenje draft avanasnog računa, tako što bi korisnik unosio podatke o avansnoj uplati, a na kraju imao opciju da odabere da sačuva ili izda račun.

Sačuvane račune možemo menjati sve do njihovog konačnog izdavanja, dok bi izmene već izdatih (avansnih) računa trebalo ograničiti, onemogućiti ili bar napisati obaveštenje da po tom i tom članu Zakona o elektronskom dokumentu nije dozvoljeno menjati već izdat račun, te da korisnik svaku izmenu vrši pod punom krivičnom odgovornošću.

Kada je uplata primljena, ona je u principu primljena u punom iznosu, sa uračunatim PDV-om, tako da mi na osnovu toga računamo koji je iznos zaračunatog PDV-a. Obe informacije iskazujemo na avansnom računu.

Što se tiče specifikacije avansnog računa, skoro da je pravilo da se kao osnov za plaćanje navodi stavka "Avansna uplata po predračunu br. 123", a dosta ređe se navodi neka konkretna stavka iz same specifikacije predračuna.

#### Froma za ispravku i dodavanje

Forma bi u principu trebalo da izgleda dosta slično kao i kod predračuna. Treba da sadrži polja o klijentu, specifikaciji računa i o datumu izdavanja (što je u principu datum prijema uplate).

Prvo polje koje se popunjava je broj predračuna na osnovu koga pravimo avansni račun. Kada se on unese, imamo informacije o klijentu kojima možemo da popunimo i avansni račun.

U specifikaciji računa se automatski dodaje stavka "Avans po predračunu `broj_odabranog_predračuna`".

### (Konačni) Računi 

Račun se izdaje po izvršenom prometu (odrađena usluga). Može se kreirati na više načina:

- Direktnom konverzijom iz predračuna - ako je bilo avansa po tom predračunu i njih uračunavamo
- ? Odabirom jednog ili više avansnih računa i kreiranjem konačnog računa na osnovu njih i odgovarajućeg predračuna
- Sa početne strane odabirom opcije `novi račun`
- Sa stranice `računi` klikom da dugme `novi račun`
- ...

U svakom slučaju se račun kreira na osnovu nekog predračuna, čak i kad direktno idemo na opciju `novi račun` u pozadini će se kreirati unos u tabeli `Predracun` (iako ga nismo eksplicitno napravili pre izdavanja računa).

Kada unesemo podatke o klijentu trebalo bi da imamo opciju da sačuvamo te podatke u bazu, zarad potencijalno ponovnog korišćenja u budućim računima.

Kada pravimo račun od prethodno napravljenog predračuna, u slučaju da je bilo avansnih uplata po tom predračunu, dobijamo obaveštenje o tome da će se praviti konačni račun sa tim avansima.

Treba onemogućiti izmene za izdate račune, dok draftove možemo menjati do trenutka izdavanja.

Razlika u odnosu na formu za predračun je u tome što forma za pravljenje i izmene računa sadrži i polja za datume i mesta prometa i izdavanja računa (kod predračuna smo imali samo datum izdavanja predračuna).

**Treba omogućiti i izmene valute, jezika i formata datuma za pojedinačne račune koje ćemo izdavati inostranim klijentima.** U tom slučaju se ne obračunava PDV:

> Prema tome, na naknadu za promet ovih usluga PDV se ne obracunava, a obv-eznik PDV ima pravo na odbitak prethodnog poreza. Dobro bi bilo, da na racunu koji se izdaje inostranom kupcu, pored uobicajenih komercijalnih podataka, navesti i: Napomenu o poreskom oslubodenju: oslobodeno placanja PDV po clanu 12. stav /npr. 3. tacka 4. podtacka 7 Zakona.

### Podešavanje naloga

Korisnik ima mogućnost izmene svojih korisničkih podataka:

- LOGIN email
- LOGIN šifra
- Adresa sedišta
- Kontakt telefon
- Kontakt email
- Tip identifikacije odgovornog lica
- PDV obveznik (DA/NE)

Dodatna podešavanja:

- Format datuma
- Format i izgled (pred)računa
- Numeracija svakog tipa računa 
- Poreski period (mesec ili tromesečje)
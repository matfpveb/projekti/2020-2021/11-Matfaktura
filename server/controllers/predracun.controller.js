const db = require("../models");
const nodemailer = require('nodemailer');
const Predracun = db.predracun;

exports.predracuniInfo = (req, res) => {
    var neplaceno = 0;
    var placeno = 0;
    req.predracuni.forEach(predracun => {
        if (!!predracun.racun && !!predracun.racun.datum_prometa) {
            placeno += predracun.cena;
        }
        else {
            neplaceno += predracun.cena;
        }
    });
    var predracuni_info = {
        broj_predracuna: req.predracuni.length,
        placeno: placeno,
        neplaceno: neplaceno,
    }
    res.status(200).json(predracuni_info);
};

exports.dodajPredracun = async (req, res) => {
    try {
        var nalog = req.nalog;
        nalog.max_predracun_broj += 1;
        await nalog.save();

        var klijent = nalog.klijenti.find(x => x._id == req.body.formData.klijent_id);
        var predracun = new Predracun({
            broj: nalog.max_predracun_broj,
            nalog_id: nalog._id,
            nalog_info: {
                naziv: nalog.naziv,
                maticni_broj: nalog.maticni_broj,
                pib: nalog.pib,
                telefon: nalog.telefon,
                email: nalog.email,
                website: nalog.website,
                adresa: nalog.adresa,
                postanski_broj: nalog.postanski_broj,
                grad: nalog.grad,
                tip_identifikacije: nalog.tip_identifikacije,
                pdv_obveznik: nalog.pdv_obveznik,
                bankovni_racuni: nalog.bankovni_racuni,
                logoUrl: nalog.logoUrl
            },
            klijent: {
                naziv: klijent.naziv,
                maticni_broj: klijent.maticni_broj,
                pib: klijent.pib,
                ime_zastupnika: klijent.ime_zastupnika,
                adresa: klijent.adresa,
                postanski_broj: klijent.postanski_broj,
                grad: klijent.grad,
                zemlja: klijent.zemlja,
                telefon: klijent.telefon,
                email: klijent.email
            },
            lista_usluga: [],
            izdat: req.body.izdavanjeData.izdat,
            datum_izdavanja: req.body.izdavanjeData.datum_izdavanja,
            mesto_izdavanja: req.body.izdavanjeData.mesto_izdavanja,
            rok_placanja: req.body.izdavanjeData.rok_placanja,
            napomena: req.body.izdavanjeData.napomena,
        });
        req.body.formData.usluge.forEach(usluga => {
            predracun.lista_usluga.push({
                proizvod_usluga_info: {
                    naziv: usluga.naziv,
                    opis: usluga.opis,
                    cena_po_jedinici_mere: usluga.cenaJm,
                    jedinica_mere: usluga.jm,
                    pdv: usluga.stopaPdv
                },
                kolicina: usluga.kolicina,
                popust: usluga.popust
            });
        });

        predracun.cena = predracun.calculateCena();
        await predracun.save();
        res.status(200).json({ message: 'Uspesno dodata usluga.' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: error.message });
    }
}

exports.izdajPostojeciPredracun = async (req, res) => {
    const predracunId = req.body.id;
    const predracuni = req.predracuni;
    var predracun = null;
    if (predracun = predracuni.find(x => x._id == predracunId)) {
        try {
            predracun.izdat = req.body.izdavanjeData.izdat;
            predracun.datum_izdavanja = req.body.izdavanjeData.datum_izdavanja;
            predracun.mesto_izdavanja = req.body.izdavanjeData.mesto_izdavanja;
            predracun.rok_placanja = req.body.izdavanjeData.rok_placanja;
            predracun.napomena = req.body.izdavanjeData.napomena;

            await predracun.save();
            res.status(200).json({ message: 'Uspesno dodata usluga.' });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: error.message });
        }
    }
}

exports.getPredracuni = (req, res) => {
    var neplaceno = 0;
    var placeno = 0;
    var vrednost_predracuna = 0;
    var racuni = [];
    req.predracuni.forEach(predracun => {
        if (!!predracun.racun && !!predracun.racun.datum_prometa) {
            placeno += predracun.cena;
        }
        else {
            neplaceno += predracun.cena;
        }
        // if (predracun.racun) {
        //     if (predracun.racun.placeno == predracun.racun.ukupno_za_placanje) {
        //         placeno += predracun.racun.placeno;
        //     }
        //     else {
        //         neplaceno += predracun.racun.ukupno_za_placanje;
        //     }
        // }
        vrednost_predracuna += predracun.cena;
        if (predracun.racun) racuni.push(predracun.racun);
    });
    var predracuni_info = {
        predracuni: req.predracuni,
        broj_predracuna: req.predracuni.length,
        placeno: placeno,
        neplaceno: neplaceno,
        vrednost_predracuna: vrednost_predracuna,
        svi_racuni: racuni,
    }

    // console.log(predracuni_info.svi_racuni);

    res.status(200).json(predracuni_info);
}

exports.getRacuni = (req, res) => {
    var neplaceno = 0;
    var placeno = 0;
    var racuni = [];
    req.predracuni.forEach(predracun => {
        if (!!predracun.racun && !!predracun.racun.datum_prometa) {
            placeno += predracun.cena;
        }
        else {
            neplaceno += predracun.cena;
        }
        if (predracun.racun) racuni.push(predracun.racun);
    });

    res.status(200).json(racuni);
}

exports.getPredracunById = (req, res) => {
    const predracunId = req.params.id;
    const predracuni = req.predracuni;
    var predracun = null;
    if (predracun = predracuni.find(x => x._id == predracunId)) {
        var predracunFormatted = {
            broj: predracun.broj,
            klijent_info: {
                naziv: predracun.klijent.naziv,
                adresa: predracun.klijent.adresa,
                maticni_broj: predracun.klijent.maticni_broj,
                pib: predracun.klijent.pib,
                postanski_broj: predracun.klijent.postanski_broj,
                grad: predracun.klijent.grad,
                telefon: predracun.klijent.telefon,
                email: predracun.klijent.email,
            },
            usluge: predracun.lista_usluga,
            izdat: predracun.izdat,
            datum_izdavanja: predracun.datum_izdavanja,
            mesto_izdavanja: predracun.mesto_izdavanja,
            rok_placanja: predracun.rok_placanja,
            napomena: predracun.napomena,
            racun: predracun.racun,
        }
        res.status(200).json(predracunFormatted);
    } else {
        res.status(404).send({ message: "Predračun sa prosleđenim id nije pronađen!" })
    }
}

exports.obrisiPredracun = async (req, res) => {
    try {
        var predracun = req.predracuni.find(p => { return p._id == req.params.id });
        await predracun.remove();
        res.status(200).json({ message: "Uspesno obrisan predracun." })
    } catch (e) {
        res.status(500).json(e);
    }
}

exports.izdajRacunIzPredracuna = async (req, res) => {
    const predracunId = req.body.id_predracuna;
    const predracuni = req.predracuni;
    var predracun = null;
    if (predracun = predracuni.find(x => x._id == predracunId)) {
        try {
            var nalog = req.nalog;
            nalog.max_racun_broj += 1;
            await nalog.save();

            predracun.izdat = true;
            predracun.racun = req.body.racun;
            predracun.racun.broj = nalog.max_racun_broj;
            await predracun.save();
            res.status(200).json({ message: 'Uspesno dodat racun.' });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: error.message });
        }
    }
}

exports.izdajRacunDirektno = async (req, res) => {
    try {
        var nalog = req.nalog;
        nalog.max_predracun_broj += 1;
        nalog.max_racun_broj += 1;
        await nalog.save();

        // console.log(nalog.max_racun_broj);

        var klijent = nalog.klijenti.find(x => x._id == req.body.formData.klijent_id);
        var predracun = new Predracun({
            broj: nalog.max_predracun_broj,
            nalog_id: nalog._id,
            nalog_info: {
                naziv: nalog.naziv,
                maticni_broj: nalog.maticni_broj,
                pib: nalog.pib,
                telefon: nalog.telefon,
                email: nalog.email,
                website: nalog.website,
                adresa: nalog.adresa,
                postanski_broj: nalog.postanski_broj,
                grad: nalog.grad,
                tip_identifikacije: nalog.tip_identifikacije,
                pdv_obveznik: nalog.pdv_obveznik,
                bankovni_racuni: nalog.bankovni_racuni,
                logoUrl: nalog.logoUrl
            },
            klijent: {
                naziv: klijent.naziv,
                maticni_broj: klijent.maticni_broj,
                pib: klijent.pib,
                ime_zastupnika: klijent.ime_zastupnika,
                adresa: klijent.adresa,
                postanski_broj: klijent.postanski_broj,
                grad: klijent.grad,
                zemlja: klijent.zemlja,
                telefon: klijent.telefon,
                email: klijent.email
            },
            lista_usluga: [],
            racun: {
                broj: nalog.max_racun_broj,
                mesto_izdavanja: req.body.formData.krMestoIzdavanja,
                datum_izdavanja: req.body.formData.krDatumIzdavanja,
                mesto_prometa: req.body.formData.krMestoPrometa,
                datum_prometa: req.body.formData.krDatumPrometa,
                rok_placanja: req.body.formData.krRokPlacanja,
                napomena: req.body.formData.krNapomena,
            },
            izdat: true,
            datum_izdavanja: req.body.formData.krMestoIzdavanja,
            mesto_izdavanja: req.body.formData.krDatumIzdavanja,
        });
        req.body.formData.usluge.forEach(usluga => {
            predracun.lista_usluga.push({
                proizvod_usluga_info: {
                    naziv: usluga.naziv,
                    opis: usluga.opis,
                    cena_po_jedinici_mere: usluga.cenaJm,
                    jedinica_mere: usluga.jm,
                    pdv: usluga.stopaPdv
                },
                kolicina: usluga.kolicina,
                popust: usluga.popust
            });
        });

        predracun.cena = predracun.calculateCena();
        predracun.racun.ukupno_za_placanje = predracun.calculateCena();
        await predracun.save();
        res.status(200).json({ message: 'Uspesno izdat račun.' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: error.message });
    }
}

exports.sendEmail = async (req, res) => {
    const klijentNaziv = req.body.klijent.ime_klijenta;
    const klijentEmail = req.body.klijent.email;

    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
            user: 'matffaktura@gmail.com',
            pass: 'MATFaktura$1$',
        }
    });

    let mailOptions = {
        from: "matffaktura@gmail.com",
        to: `${klijentEmail}`,
        subject: "MATFAKTURA - Dobrodošlica",
        html: "\
        <div style='background-color: #FBF9EF;padding: 40px 30px;'>\
        <hr style='background-color: black;height: 1px;'>\
            <h2 style='text-align:center;'>MATFAKTURA</h2> \
            <p style='text-align:center;'>Dobro došli! Uspešno ste se registrovali na sistem Matfaktura!</p>\
            <p style='text-align:center;'>Hvala Vam što ste izabrali naš sistem za fakture.\
            <h3>Pregled mogućnosti sistema:</h3>\
            <ul>\
                <li>Evidencija klijenata</li>\
                <li>Evidencija usluga</li>\
                <li>Evidencija predračuna i računa</li>\
                <li>Izdavanje predračuna i računa</li>\
                <li>Analitika poslovanja firme (pregled zaduženja klijenata, pregled najtraženijih usluga)</li>\
            </ul>\
            <br>\
            <p>Za bilo kakvu nedoumicu, pišite nam na <a href='mailto:matffaktura@gmail.com'>matffaktura@gmail.com</a>!</p>\
            <br>\
            <p style='text-align: right'>Pozdrav, <br> <b style='color: #f95738'>Matfaktura Tim</b></p>\
            <hr style='background-color: black;height: 1px;'>\
        </div>\
        "
    };


    let info = await transporter.sendMail(mailOptions);

}

exports.racunPlacen = async (req, res) => {
    const predracunId = req.body.racunId;
    const predracuni = req.predracuni;
    var predracun = null;
    if (predracun = predracuni.find(x => x._id == predracunId)) {
        try {
            console.log(predracun);
            predracun.racun.placeno = predracun.racun.ukupno_za_placanje;
            await predracun.save();
            console.log(predracun);
            res.status(200).json({ message: 'Uspesno registrovana uplata.'});
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: error.message });
        }
    }
}
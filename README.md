# Project Matfaktura

Application for issuance of invoices, accounts, accountings.
## Developers

- [Ikovic Svetozar, 69/2016](https://gitlab.com/toswe)
- [Dimitrije Stamenić, 260/2016](https://gitlab.com/stamd)
- [Mihajlo Zivkovic, 87/2016](https://gitlab.com/mi16087)
- [Lazar Ristić, 150/2016](https://gitlab.com/lristic)
- [Dimitrije Petrović, 466/2017](https://gitlab.com/mitokv)

## Requirements

You will need [node.js and npm](https://www.geeksforgeeks.org/installation-of-node-js-on-linux/), and [mongodb](https://docs.mongodb.com/manual/administration/install-community/) to run this app.
Then you need to install:
```shell
npm install -g @angular/cli
```

## Init project

Clone the repository, move to the cloned directory, install required packages
```shell
git clone git@gitlab.com:matfpveb/projekti/2020-2021/11-Matfaktura.git matfaktura
cd 11-Matfaktura
npm install
```

position yourself into server directory of root directory and install node packages:
```shell
cd server
npm install
```

position yourself into angular directory of root directory and install angular packages:
```shell
cd angular
npm install
```

## Running the project
### Running the server

position yourself into server directory and run the following command:
```shell
nodemon server.js
```

### Run the Application

position yourself into angular directory and run the following command:
```shell
ng serve --open
```

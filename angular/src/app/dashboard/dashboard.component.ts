import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { ChartsModule, Color, Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

const API_URL = 'http://localhost:8080/api'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  [x: string]: any;

  
  nalog_info: any;
  predracuni_info: any;
  currentUkupno: any;
  listaUsluga: string[] = [];
  listaCount: any;
  sviKlijenti: string[] = [];
  cene: Array<number> = [];

  // Bar Chart
  barChartData: ChartDataSets[] = [];
  barChartDataArray: Array<number> = [];
  barChartLabels: Label[] = [];
  barChartLegend = true;
  barChartType: ChartType = 'bar';
  barChartOptions: ChartOptions = {};

  // Pie Chart

  pieChartLabels: Label[] = [];
  pieChartData: number[] = [];
  pieChartType: ChartType = 'pie';
  pieChartPlugins = [pluginDataLabels];
  pieChartLegend = true;
  pieChartColors = [
    {
      backgroundColor: ['rgba(249, 87, 56, .8)', 'rgba(39, 52, 105, .8)', 'rgba(0,0,255,0.3)'],
    },
  ];
  pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels![ctx.dataIndex];
          return label;
        },
      },
    }
  };

  // Line Chart
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';


  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(API_URL + '/nalog').subscribe(x => { this.nalog_info = x; });
    this.http.get(API_URL + '/predracuni')
      .subscribe(
        x => {
          // fill bar chart
          this.predracuni_info = x;
          let index = 0;
          this.predracuni_info.predracuni.forEach((el: any) => {
            this.currentUkupno = 0;
            const i = this.sviKlijenti.indexOf(el.klijent.naziv);
            if (i === -1) { // push unique clients
              this.sviKlijenti.push(el.klijent.naziv);
              this.barChartData[index] = { data: el.cena, label: 'Neplaćeno' };
              index++;
            } else {
              const data = this.barChartData[i].data + el.cena;

              this.barChartData[i] = { data: data, label: 'Neplaćeno' };
            }

            // fill usluge pie chart
            el.lista_usluga.forEach((u: any) => {
              this.listaUsluga.push(u.proizvod_usluga_info.naziv);
            });
          });

          const listaCount = new Map([...new Set(this.listaUsluga)].map(
            x => [x, this.listaUsluga.filter(y => y === x).length]
          ));

          this.pieChartData = Array.from(listaCount.values());
          this.pieChartLabels = Array.from(listaCount.keys());

          if (this.barChartData.length == 0) {
            this.barChartData = [{ data: [], label: 'Neplaceno' }]
          }
          this.barChartLabels = this.sviKlijenti;


          this.animateValue(document.getElementById("neplaceno"), 0, this.predracuni_info.neplaceno, 1000);
          this.animateValue(document.getElementById("placeno"), 0, this.predracuni_info.placeno, 1000);
          this.animateValue(document.getElementById("broj_racuna"), 0, this.predracuni_info.predracuni.length, 1000);
          this.animateValue(document.getElementById("broj_klijenata"), 0, this.nalog_info.broj_klijenata, 1000);

        }
      );
    this.barChartOptions = {
      responsive: true,
      scales: {
        xAxes: [{}], yAxes: [{
          ticks: {
            beginAtZero: true,
            max: this.predracuni_info.neplaceno
          }
        }]
      },
      plugins: {
        datalabels: {
          anchor: 'end',
          align: 'end',
        }
      }
    }
  }

  animateValue(obj: any, start: any, end: any, duration: number) {
    let startTimestamp: any = null;
    const step = (timestamp: any) => {
      if (!startTimestamp) startTimestamp = timestamp;
      const progress = Math.min((timestamp - startTimestamp) / duration, 1);
      if (obj.id == "placeno" || obj.id == "neplaceno") {
        obj.innerHTML = parseFloat(progress * (end - start) + start).toLocaleString('en-GB');


        if (progress < 1) {
          window.requestAnimationFrame(step);
        }
      } else {
        obj.innerHTML = Math.floor(progress * (end - start) + start);
        if (progress < 1) {
          window.requestAnimationFrame(step);
        }
      }

    };
    window.requestAnimationFrame(step);
  }

}

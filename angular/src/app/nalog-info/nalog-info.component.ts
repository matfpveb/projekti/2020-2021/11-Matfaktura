import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

const API_URL = 'http://localhost:8080/api/';

@Component({
  selector: "app-nalog-info",
  templateUrl: "./nalog-info.component.html",
  styleUrls: ["./nalog-info.component.css"]
})
export class NalogInfoComponent implements OnInit {
  nalogInfoUpdated = false;
  bankovniRacunUpdated = false;
  sviBankovniRacuni: any;

  nalogInfoForm: FormGroup;
  nalogInfo: any;
  pdvObveznik: any;
  logoImage!: File;

  bankovniRacuniForm: FormGroup;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
    
    this.nalogInfoForm = new FormGroup({
      naziv: new FormControl({ value: "" }, [Validators.required]),
      maticni_broj: new FormControl({ value: "", disabled: true }, [
        Validators.required
      ]),
      pib: new FormControl({ value: "", disabled: true }, [
        Validators.required
      ]),
      adresa: new FormControl({ value: "" }, [Validators.required]),
      postanski_broj: new FormControl({ value: "" }, [Validators.required]),
      grad: new FormControl({ value: "" }, [Validators.required]),
      telefon: new FormControl({ value: "" }, [Validators.required]),
      email: new FormControl({ value: "" }, [Validators.required]),
      website: new FormControl({ value: "" }, [Validators.required]),
      pdv_obveznik: new FormControl({ value: "" }, [Validators.required])
    });

    this.bankovniRacuniForm = this.fb.group({
      bankovni_racuni: this.fb.array([])
    });
  }

  ngOnInit(): void {
    this.http.get(API_URL + "bankovni-racuni").subscribe(x => {
      this.sviBankovniRacuni = x;
      this.sviBankovniRacuni.forEach((el: any) => {
        this.bankovniRacuni.push(
          this.fb.group({
            _id: el._id,
            broj_racuna: el.broj_racuna,
            valuta: el.valuta
          })
        );
      });
    });

    this.fillNalogInfoForm();
  }

  onSubmit(): void {
    if (this.nalogInfoForm.invalid) {
      window.alert("Nevalidna forma!");
      return;
    }
    if (confirm("Da li ste sigurni da želite da sačuvate izmene?")) {
      if (this.logoImage) {
        this.onUpload();
      }
      this.http
        .patch(API_URL + "nalog", this.nalogInfoForm.value)
        .subscribe(data => {
          this.nalogInfoUpdated = true;
          setTimeout(() => {
            this.nalogInfoUpdated = false;
          }, 2000);
        });
      window.location.reload();
    }
  }

  fillNalogInfoForm(): void {
    this.http.get(API_URL + "nalog").subscribe(
      x => {
        this.nalogInfoForm.patchValue(x);

        this.nalogInfo = x;

        this.pdvObveznik = this.nalogInfo.pdv_obveznik;
      },
      err => {
        window.location.href = "/404";
      }
    );

  }

  get bankovniRacuni() {
    return this.bankovniRacuniForm.get("bankovni_racuni") as FormArray;
  }

  addBankovniRacun() {
    this.bankovniRacuni.push(this.fb.group({ broj_racuna: "", valuta: "" }));
  }

  saveBankovniRacun(index: any) {
    this.http
      .patch(
        API_URL + "bankovni-racuni/dodaj",
        this.bankovniRacuni.at(index).value
      )
      .subscribe();
    this.bankovniRacunUpdated = true;
    setTimeout(() => {
      this.bankovniRacunUpdated = false;
    }, 3000);
    this.sviBankovniRacuni.removeAt(index);
    this.http.get(API_URL + "bankovni-racuni").subscribe(x => {
      this.sviBankovniRacuni = x;
      this.sviBankovniRacuni.forEach((el: any) => {
        this.bankovniRacuni.push(
          this.fb.group({
            _id: el._id,
            broj_racuna: el.broj_racuna,
            valuta: el.valuta
          })
        );
      });
    });
  }

  deleteBankovniRacun(index: any) {
    if (confirm("Da li ste sigurni da želite da obrišete bankovni račun?")) {
      this.http
        .patch(
          API_URL + "bankovni-racuni/obrisi",
          this.bankovniRacuni.at(index).value
        )
        .subscribe();
      this.bankovniRacuni.removeAt(index);
    }
  }

  changeSistemPDV(event: any) {

    this.http
      .patch(API_URL + "nalog/pdv", { pdv_obveznik: event.target.checked })
      .subscribe();
    this.http.get(API_URL + "nalog").subscribe();
  }

  onFileChanged(event: any): any {

    this.logoImage = event.target.files[0];

  }

  onUpload(): any {
    let uploadData = new FormData();

    uploadData.append('file', this.logoImage);


    let headers = new HttpHeaders();
    headers.append("enctype", "multipart/form-data");

    this.http.patch(API_URL + 'nalog/logo', uploadData, { headers: headers }).subscribe(
      data => {
        this.nalogInfoUpdated = true;
      });


  }

  public nazivHasErrors(): string {
    return this.fieldHasErrors("naziv") ? "alert-danger" : "";
  }

  public maticniBrojHasErrors(): string {
    return this.fieldHasErrors("maticni_broj") ? "alert-danger" : "";
  }

  public pibHasErrors(): string {
    return this.fieldHasErrors("pib") ? "alert-danger" : "";
  }

  public websiteHasErrors(): string {
    return this.fieldHasErrors("website") ? "alert-danger" : "";
  }

  public postanskiBrojHasErrors(): string {
    return this.fieldHasErrors("postanski_broj") ? "alert-danger" : "";
  }

  public gradHasErrors(): string {
    return this.fieldHasErrors("grad") ? "alert-danger" : "";
  }

  public zemljaHasErrors(): string {
    return this.fieldHasErrors("zemlja") ? "alert-danger" : "";
  }

  public telefonHasErrors(): string {
    return this.fieldHasErrors("telefon") ? "alert-danger" : "";
  }

  public adresaHasErrors(): string {
    return this.fieldHasErrors("adresa") ? "alert-danger" : "";
  }

  public emailHasErrors(): string {
    return this.fieldHasErrors("email") ? "alert-danger" : "";
  }

  private fieldHasErrors(fieldName: string): boolean {
    const control: AbstractControl | null = this.nalogInfoForm.get(fieldName);
    if (control === null) {
      return false;
    }
    const errors: ValidationErrors | null = control.errors;
    return errors != null;
  }

  nazivErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("naziv");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Naziv klijenta je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  maticniBrojErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get(
      "maticni_broj"
    );
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Maticni broj je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  pibErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("pib");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Pib je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  websiteErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("website");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Website je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  postanskiBrojErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get(
      "postanski_broj"
    );
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Postanski broj je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  gradErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("grad");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Grad je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  zemljaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("zemlja");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Zemlja je obavezna.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  telefonErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("telefon");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Telefon je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  adresaErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("adresa");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Adresa je obavezna.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  emailErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.nalogInfoForm.get("email");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Kontakt e-mail je obavezan.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }
}

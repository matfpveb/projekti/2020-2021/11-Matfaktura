# Matfakturadb

## Uvoz/izvoz

```shell
cd server/database
```
### Uvoženje Database

```shell
sh importdb.sh
```

### Izvoženje Database
```shell
sh exportdb.sh
```

## Izgled Databaze Matfakturadb

Baza Matfakturadb se sastoji od dve kolekcije:
- nalozi
- predracuni

### Nalozi:
```shell
    _id: ObjectId
    login_email: String
    password: String
    naziv: String
    maticni_broj: String
    pib: String
    telefon: String
    email: String
    website: String
    adresa: String
    postanski_broj: String
    grad: String
    tip_identifikacije: String
    pdv_obveznik: Boolean
    max_predracun_broj: Number
    logoUrl: String
    bankovni_racuni: Array
        Object: 
            _id: ObjectId
            broj_racuna: String
            valuta: String
    klijenti: Array
        Object:
            _id: ObjectId
            naziv: String
            maticni_broj: String
            pib: String
            ime_zastupnika: String
            adresa: String
            postanski_broj: String
            grad: String
            zemlja: String
            telefon: String
            email: String
    proizvod_usluga: Array
        Object:
            _id: ObjectId
            naziv: String
            opis: String
            cena_po_jedinici_mere: Number
            jedinica_mere: String
            pdv: Number
```
### Predracuni:
```shell
    _id: ObjectId
    broj: String
    nalog_id: mongoose.Schema.Types.ObjectId
    nalog_info: Object
        naziv: String
        maticni_broj: String
        pib: String
        telefon: String
        email: String
        website: String
        adresa: String
        postanski_broj: String
        grad: String
        tip_identifikacije: String
        pdv_obveznik: Boolean
        bankovni_racuni: Object
            broj_racuna: String
            valuta: String
    klijent: Object
        naziv: String
        maticni_broj: String
        pib: String
        ime_zastupnika: String
        adresa: String
        postanski_broj: String
        grad: String
        zemlja: String
        telefon: String
        email: String
    datum_izdavanja: Date
    mesto_izdavanja: String
    rok_placanja: Date
    napomena: String
    izdat: Boolean
    cena: Number
    lista_usluga: Array
        Objetc:
            _id: ObjectId
            proizvod_usluga_info: Object
                naziv: String
                opis: String
                cena_po_jedinici_mere: Number
                jedinica_mere: String
                pdv: Number
            kolicina: Number
            popust: Number
            
    avansni_racuni: Array
        Object:
            _id: ObjectId
            broj: String
            mesto_izdavanja: String
            datum_izdavanja: Date
            iznos: Number
            pdv: Number
            izdat: Boolean
            zatvoren: Boolean
    racun: Object
        broj: String
        mesto_izdavanja: String
        datum_izdavanja: String
        mesto_prometa: String
        datum_prometa: String
        rok_placanja: String
        placeno: Boolean
        ukupno_za_placanje: Number
```
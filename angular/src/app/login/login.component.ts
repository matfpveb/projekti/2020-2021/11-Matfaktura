import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from "@angular/forms";
import { AuthService } from "../_services/auth.service";
import { TokenStorageService } from "../_services/token-storage.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = "";
  isSuccessful = false;

  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private http: HttpClient,
    private router: Router
  ) {
    this.loginForm = new FormGroup({
      login_email: new FormControl("", [
        Validators.required,
        Validators.pattern(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/),
        Validators.minLength(3),
        Validators.maxLength(50)
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.router.navigate(["dashboard"]);
    }
  }

  public loginEmailHasError(): string {
    return this.fieldHasErrors("login_email") ? "alert-danger" : "";
  }

  public passwordHasError(): string {
    return this.fieldHasErrors("password") ? "alert-danger" : "";
  }

  private fieldHasErrors(fieldName: string): boolean {
    const control: AbstractControl | null = this.loginForm.get(fieldName);
    if (control === null) {
      return false;
    }
    const errors: ValidationErrors | null = control.errors;
    return errors != null;
  }

  loginEmailErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.loginForm.get("login_email");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Login e-mail je obavezan.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Login e-mail mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
          `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (errors.maxlength) {
      errorMessages.push(
        `Login e-mail može da sadrži najviše  ${errors.maxlength.requiredLength} karaktera. ` +
          `Uneli ste ${errors.maxlength.actualLength} karaktera.`
      );
    }

    if (errors.pattern) {
      errorMessages.push("Login e-mail je pogrešnog formata.");
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  passwordErrors(): string[] {
    const errorMessages: string[] = [];
    const control: AbstractControl | null = this.loginForm.get("password");
    if (control === null) {
      return errorMessages;
    }
    const errors: ValidationErrors | null = control.errors;

    if (errors === null) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Lozinka je obavezna.");
    }

    if (errors.minlength) {
      errorMessages.push(
        `Lozinka mora da sadrži najmanje  ${errors.minlength.requiredLength} karaktera. ` +
          `Uneli ste ${errors.minlength.actualLength} karaktera.`
      );
    }

    if (!control.dirty && !control.touched) {
      return [];
    }

    return errorMessages;
  }

  onSubmit(): void {
    if (this.loginForm.invalid) {
      window.alert("Nevalidna forma!");
      return;
    }
    const { login_email, password } = this.loginForm.value;

    this.authService.login(login_email, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;

        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage(): void {
    window.location.href = "/";
  }
}

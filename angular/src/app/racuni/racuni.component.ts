import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

const API_URL = 'http://localhost:8080/api/'

@Component({
  selector: 'app-racuni',
  templateUrl: './racuni.component.html',
  styleUrls: ['./racuni.component.css']
})
export class RacuniComponent implements OnInit {

  page = 1;
  count = 0;
  tableSize = 3;
  tableSizes = [3, 6, 9, 12];

  naslov = "";
  naslovJednina = "";
  tipRacuna: any;
  predracuni: any;
  currentNalog: any;

  
  

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
   }

  ngOnInit(): void {
    this.http.get(API_URL + "nalog").subscribe(x=>{
      this.currentNalog = x;
    });

    this.http.get(API_URL + 'predracuni').subscribe(x => {
      this.predracuni = x;
      
    });

    this.tipRacuna = this.route.snapshot.paramMap.get('tip');
    if (this.tipRacuna) {
      this.initPage();
    }
  }

  initPage(): void {
    if (this.tipRacuna == "kr") {
      this.naslov = "Računi";
      this.naslovJednina = "račun";
    } else if (this.tipRacuna == "pr") {
      this.naslov = "Predračuni";
      this.naslovJednina = "predračun";
    } else {
      window.location.href = "/404";
    }
  }

  deleteItem(id: any): void {
    if (confirm("Da li ste sigurni da želite da obrišete račun?")) {
      this.http.delete(API_URL + 'predracun/' + id).subscribe(response => {
        
        this.http.get(API_URL + 'predracuni').subscribe(x => this.predracuni = x);
      });
    }
  }

  onTableDataChange(event: any) {
    this.page = event;
    this.http.get(API_URL + 'predracuni').subscribe(x => this.predracuni = x);
  }

  onTableSizeChange(event: any): void {
    this.tableSize = event.target.value;
    this.page = 1;
    this.http.get(API_URL + 'predracuni').subscribe(x => this.predracuni = x);
  }
}
